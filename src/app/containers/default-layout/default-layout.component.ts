import { Component, Inject, HostListener } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core'
import { navItems } from './../../_nav';
import { navReader } from './../../_navreader';
// import { navInfluencer } from './../../_navInFluencer';
import { environment } from '../../../environments/environment'
import { Subscription } from 'rxjs/Subscription';
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { e } from '@angular/core/src/render3';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  // urlenv = environment.apiUrl;
  user = localStorage.getItem('user')

  imgicon = "assets/img/brand/digivla white.png"

  logo = { src: ('https://admin.antara-insight.id/asset/images/' + localStorage.getItem('logo')), width: 50, height: 35, alt: localStorage.getItem('user') }
  // logo = {src: ('http://localhost:3000' + localStorage.getItem('logo')) + '.jpg', width: 50, height: 35, alt: localStorage.getItem('user')}

  url = environment.apiUrl;
  logo_img: string = "assets/img/brand/digivla white.png";

  menuLevel = localStorage.getItem('levelmenu')
  // menuLevel='2' //testt
  reader = '5';
  admin = '3';
  editor = '2';
  viewer = '1';

  nextValue;
  page = 1;

  groupCategoryOption: any[] = [];
  groupCategoryModel = '0';

  subCategoryOption: any[] = [];
  subCategoryModel = 'All Sub Categroy';

  groupMediaOption: any[] = [];
  groupMediaModel = '0';

  subMediaOption: any[] = [];
  subMediaModel = '0';

  periodModel: string = '1';
  today = new Date();
  periodFromModel: Date = this.addDays(this.today, -1);
  periodEndModel: Date = this.today;

  edited: boolean = false;
  bsValue: Date = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  domain: string;

  togle = true;// for to slide bar 

  influncerNiyee: any[] = [];
  // public navItems = navItems;
  public navItems;
  public sidebarMinimized = true;
  text_footer: any;
  url_footer: any;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  progressRef: NgProgressRef

  // parentSubject:Subject<any> = new Subject();

  notifyChildren() {
    this.childListener.notifyOther({ option: 'call_child', value: 'From child' });
  }

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token')
  });
  options = { headers: this.headers };

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  logOutHandler() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

  onChange(deviceValue) {
    if (deviceValue == 1) {
      this.periodFromModel = this.addDays(this.today, -1);
      this.periodEndModel = this.today;
      this.edited = false;
    } else if (deviceValue == 2) {
      this.periodFromModel = this.addDays(this.today, -7);
      this.periodEndModel = this.today;
      this.edited = false;
    } else if (deviceValue == 3) {
      this.periodFromModel = this.addDays(this.today, -30);
      this.periodEndModel = this.today;
      this.edited = false;
    } else if (deviceValue == 4) {
      this.periodFromModel = this.addDays(this.today, -360);
      this.periodEndModel = this.today;
      this.edited = false;
    } else if (deviceValue == 5) {
      this.edited = true;
    }
  }

  onGroupCategoryChange(groupCategoryValue) {
    this.getSubCategory();
  }
  constructor(private http: HttpClient, public progress: NgProgress, private childListener: ChildListenerService, private router: Router) {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized')
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  onGroupMediaChange(groupMediaValue) {
    this.getSubMedias();
  }

  getGroupCategory() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/categories/', this.options)
      .subscribe((result: any) => {
        this.groupCategoryOption = result.results;
        if (localStorage.getItem('client') === 'tkgaa'){
          result.results.forEach((element,index) => {
            if (element.category_set === 3085){   
              this.move(this.groupCategoryOption , index , 0)
            }
          });
        }else{
          this.groupCategoryOption.unshift({ 'client_id': result.results[0].client_id, 'category_set': '0', 'descriptionz': 'All Group Category', 'usere': result.results[0].usere, 'input_data_date': result.results[0].input_data_date, 'pc_name': result.results[0].pc_name });
        }
        this.groupCategoryModel = this.groupCategoryOption[0].category_set;
      });
  }

  move(arr, old_index, new_index) {
    while (old_index < 0) {
        old_index += arr.length;
    }
    while (new_index < 0) {
        new_index += arr.length;
    }
    if (new_index >= arr.length) {
        var k = new_index - arr.length;
        while ((k--) + 1) {
            arr.push(undefined);
        }
    }
     arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
   return arr;
}

  getSubCategory() {
    interface UserResponse {
      data: Object;
    }
    let urlExtend = 'user/subcategories/' + this.groupCategoryModel;
    if (this.groupCategoryModel == '0') {
      this.subCategoryOption = [{ 'client_id': 'sdads', 'category_set': '0', 'category_id': 'All Sub Categroy', 'default_set': '', 'usere': 'dasda', 'input_data_date': null, 'pc_name': null }];
      this.subCategoryModel = this.subCategoryOption[0].category_id;
    } else {
      this.http.get<UserResponse>(this.url + urlExtend, this.options)
        .subscribe((result: any) => {
          this.subCategoryOption = result.results;
          if (this.subCategoryOption.length < 1) {
            this.subCategoryOption.unshift({ 'client_id': this.subCategoryOption[0].client_id, 'category_set': '0', 'category_id': 'No Sub Categroy', 'default_set': '', 'usere': this.subCategoryOption[0].usere, 'input_data_date': this.subCategoryOption[0].input_data_date, 'pc_name': this.subCategoryOption[0].pc_name });
          } else {
            this.subCategoryOption.unshift({ 'client_id': this.subCategoryOption[0].client_id, 'category_set': '0', 'category_id': 'All Sub Categroy', 'default_set': '', 'usere': this.subCategoryOption[0].usere, 'input_data_date': this.subCategoryOption[0].input_data_date, 'pc_name': this.subCategoryOption[0].pc_name });
          }
          this.subCategoryModel = this.subCategoryOption[0].category_id;
        });
    }
  }

  getMediaGroup() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/medias/', this.options)
      .subscribe((result: any) => {
        this.groupMediaOption = result.results;
        this.groupMediaOption.unshift({ "client_id": "niyee", "user_media_type_id": "0", "user_media_type_name_def": "All Group Media", "input_data_date": "2018-10-22T15:06:49", "pc_name": "web-api-2", "usere": "1525055182" });
        this.groupMediaModel = this.groupMediaOption[0].user_media_type_id;
      });
  }

  getSubMedias() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/submedias/' + this.groupMediaModel + '?page=' + this.page, this.options)
      .subscribe((result: any) => {
        this.subMediaOption = result.results;
        this.nextValue = result.next;
        if (this.subMediaOption.length < 1) {
          if (this.groupMediaModel == '0') {
            this.subMediaOption.unshift({ "media_id": 0, "media_name": "All Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
          } else {
            this.subMediaOption.unshift({ "media_id": 0, "media_name": "No Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
          }
        } else {
          this.subMediaOption.unshift({ "media_id": 0, "media_name": "All Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
        }
        this.subMediaModel = this.subMediaOption[0].media_id;
      });
  }





  flagLevelmenu() {
    if (this.menuLevel == this.admin) {
      this.navItems = navItems;
    } else if (this.menuLevel == this.viewer) {
      this.navItems = navReader;
    } else if (this.menuLevel == this.editor) {
      this.navItems = navReader;
    } else if (this.menuLevel == this.reader) {
      this.navItems = navReader;
    }
  }

  onChangeMore(deviceValue) {
    if (deviceValue == 1) {
      if (this.nextValue != null) {
        this.page++;
        this.getSubMedias();
      } else if (this.nextValue == null)
        this.page--;
      this.getSubMedias();
    }

  }


  ngOnInit(): void {
    $(".bs-datepicker").css({
      'position': 'absolute !important',
      'top': '-30vh !important',
      'left': '-15vw !important'
    });
    this.text_footer = 'Digivla Indonesia'
    this.url_footer = 'http://antara-insight.id/'
    $("#logo_footer_url").css({
      'max-width': '100%'
    });
    this.progressRef = this.progress.ref();

    this.progressRef.start();
    //group category
    this.getGroupCategory();

    //sub category
    this.getSubCategory();

    //get media
    this.getMediaGroup();

    //get submedia
    this.getSubMedias();

    //flag menu
    this.flagLevelmenu();

    this.progressRef.complete();
    this.domain = window.location.hostname.split('.')[1];

    if (this.domain == 'altegra') {
      this.logo_img = 'assets/img/brand/mk.png'
    }
    switch (localStorage.getItem('client') && localStorage.getItem('user')) {
      case 'niyee' || 'snnkb':
        this.navItems = []
        this.navItems.push({ name: "Dashboard", url: "/dashboard", icon: "icon-speedometer" },
          { name: "Spokesperson", url: "/influencerdashboard", icon: "icon-user" },
          { name: "Editing", url: "/editing", icon: "cui-note icons" },
          { name: "News Clipping", url: "/newsclipping", icon: "cui-puzzle" },
          { name: "Search", url: "/search", icon: "cui-magnifying-glass" },
          { name: "Connect", url: "/connect", icon: "icon-people" },
          {
            name: "Configurations", url: "/configurations", icon: "cui-settings", children: [{ name: "Group Media", url: "/groupmedia", icon: "icon-feed" },
            { name: "Group Category", url: "/groupcategory", icon: "icon-notebook" },
            { name: "Sub Category", url: "/subcategory", icon: "icon-tag" },
            { name: "Influencer", url: "/influencer", icon: "icon-tag" }]
          })
        break;
      case 'tkgaa':
        this.navItems = []
        this.navItems.push({ name: "Dashboard", url: "/dashboard", icon: "icon-speedometer" },
          { name: "Spokesperson", url: "/influencerdashboard", icon: "icon-user" },
          { name: "Editing", url: "/editing", icon: "cui-note icons" },
          { name: "News Clipping", url: "/newsclipping", icon: "cui-puzzle" },
          { name: "Search", url: "/search", icon: "cui-magnifying-glass" },
          { name: "Connect", url: "/connect", icon: "icon-people" },
          {
            name: "Configurations", url: "/configurations", icon: "cui-settings", children: [{ name: "Group Media", url: "/groupmedia", icon: "icon-feed" },
            { name: "Group Category", url: "/groupcategory", icon: "icon-notebook" },
            { name: "Sub Category", url: "/subcategory", icon: "icon-tag" },
            { name: "Influencer", url: "/influencer", icon: "icon-tag" }]
          },
          { name: "Geospatial", url: "/geospatial", icon: "icon-globe" },
          { name: "News Trends", url: "/newstrends", icon: "icon-paper-clip" }
        )
        break;
        case 'R_JTI':
        this.navItems = []
        this.navItems.push(
          { name: "Dashboard", url: "/dashboard", icon: "icon-speedometer" },
          { name: "News Clipping", url: "/newsclipping", icon: "cui-puzzle" }
        )
        break;
        case 'KPPU':
        this.navItems = []
        this.navItems.push(
          { name: "Dashboard", url: "/dashboard", icon: "icon-speedometer" },
          { name: "News Clipping", url: "/newsclipping", icon: "cui-puzzle" }
        )
        break;
      default:
        this.navItems = []
        this.navItems.push({ name: "Dashboard", url: "/dashboard", icon: "icon-speedometer" },
          { name: "Editing", url: "/editing", icon: "cui-note icons" },
          { name: "News Clipping", url: "/newsclipping", icon: "cui-puzzle" },
          { name: "Search", url: "/search", icon: "cui-magnifying-glass" },
          { name: "Connect", url: "/connect", icon: "icon-people" },
          {
            name: "Configurations", url: "/configurations", icon: "cui-settings", children: [{ name: "Group Media", url: "/groupmedia", icon: "icon-feed" },
            { name: "Group Category", url: "/groupcategory", icon: "icon-notebook" },
            { name: "Sub Category", url: "/subcategory", icon: "icon-tag" }
            ]
          })
    }
  }
}
