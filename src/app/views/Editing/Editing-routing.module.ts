import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { EditingComponent } from './Editing.component';

const routes: Routes = [
  {
    path: '',
    component: EditingComponent,
    data: {
      title: 'Editing'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditingRoutingModule {}
