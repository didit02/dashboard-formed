import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Inject,
  TemplateRef,
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { moment } from "ngx-bootstrap/chronos/test/chain";

import { Subscription } from "rxjs/Subscription";
import { ChildListenerService } from "../../views/Services/ChildListener.service";
import { environment } from "../../../environments/environment";
import { AlertComponent } from "ngx-bootstrap/alert/alert.component";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { Template } from "@angular/compiler/src/render3/r3_ast";

@Component({
  selector: "app-Editing",
  templateUrl: "./Editing.component.html",
  styleUrls: ["./Editing.component.scss"],
})
export class EditingComponent implements OnInit, OnDestroy {
  keyword = "name";
  url = environment.apiUrl;
  urlsub = environment.apiSub;
  article_idMentionModel;
  category_idMentionModel;
  toneModel = 0;
  summaryModel;
  private subscription: Subscription;
  checkedAllModel: boolean = false;
  editingList: any = [];
  objectEditing;
  articleChecked;
  deleteShow: boolean = false;
  deleteShowsend : boolean = false;
  showpanelnew = false;
  clientURl: any = "";
  editname: string;
  getarticleidi: any;
  getarticleccategory: any;
  contenttopic: any;
  dataalltopicgetinit: any;
  flagToneSumary;
  trigersub:boolean = false;
  autocomplete = [];

  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token"),
  });
  options = { headers: this.headers };

  alerts;

  pageSize: number = 10;
  currentPage: number = 1;
  searchModel;
  isASC = false;
  topicNameModel: any = "";
  topicDetailsList = [];
  oldtopic: any;
  topicModel;
  valueselectcategory:any;
  groupCategoryOption = [];
  subCategoryOption = [];
  subCategoryModel:any;
  trigerclient:any;
  constructor(
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
    private modalService: BsModalService,
    private http: HttpClient,
    private childListener: ChildListenerService
  ) {}
  @ViewChild("searchModalContainer") public searchModalContainer;
  @ViewChild("deleteConfirmaton") public delConfirm;
  @ViewChild("editToneModal") public editToneModal;
  @ViewChild("editSummaryModal") public editSummaryModal;
  @ViewChild("template") public template;
  @ViewChild("topicModals") public topicModals;
  @ViewChild("subTopicDetail") public subTopicDetail;
  modalRef: BsModalRef;
  datachecked = [];
  groupCategoryModel:any;
  selectEvent(item) {
    // do something with selected item
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something when input is focused
    this.autocomplete = [];
    interface UserResponse {
      data: Object;
    }
    this.http
      .post<UserResponse>(
        this.parent.url + "topic/get-topic-list/",
        "",
        this.options
      )
      .subscribe(
        (result: any) => {
          result.data.forEach((element, index) => {
            let dataobj = {
              id: index + 1,
              name: element,
            };
            this.autocomplete.push(dataobj);
          });
        },
        (err: any) => {}
      );
  }
  sortingHandler(field) {
    var sort;
    if (this.isASC == false) {
      this.isASC = true;
      sort = "asc";
    } else {
      this.isASC = false;
      sort = "desc";
    }

    this.loadEditingData(1, field, sort);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.loadEditingData(event.page, "datee", "desc");
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: "modal-lg" });
  }

  openSendArticle(sendarticle: TemplateRef<any>) {
    this.modalRef = this.modalService.show(sendarticle, { class: "modal-lg" });
  }
  getGroupCategory() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>('https://apijasamarga.digivla.id/api/v1/user/categories/',this.options)
      .subscribe((result: any) => {
        this.groupCategoryOption = result.results;
        this.groupCategoryModel= result.results[0].descriptionz
        this.getSubCategory(this.groupCategoryModel)
      });
  }
  onGroupCategoryChange(text) {
    this.valueselectcategory = text;
    this.getSubCategory(this.valueselectcategory);
  }
  getSubCategory(text) {  
    this.groupCategoryOption.forEach(element => {
      if (element.descriptionz === text) {
        interface UserResponse {
          data: Object;
        }
        let urlExtend = 'https://apijasamarga.digivla.id/api/v1/user/subcategories/' + element.category_set;
          this.http.get<UserResponse>(urlExtend, this.options)
            .subscribe((result: any) => {
              let data = [];
                result.results.forEach(element => {
                    if (element.category_id !== 'All Korporasi'){
                      data.push(element)
                    }
                });
              this.subCategoryOption = data
              this.subCategoryModel = this.subCategoryOption[0].category_id;
            });
      }
    });
  }
  sendarticleselect(){
    if (this.subCategoryModel === 'all'){
      this.trigersub = true
      setTimeout(() => {
        this.trigersub = false
      }, 5000);
    }else{
      let dataparams  = {
        category_id : this.groupCategoryModel,
        sub_category_id : this.subCategoryModel,
        articles:this.datachecked
      }
      interface UserResponse {
        data: Object;
      }
      this.http.post<UserResponse>(this.urlsub+'user/editing/add-article-jm',dataparams, this.options)
        .subscribe((result: any) => {
          if (result.message === 'add Article Success'){
                this.datachecked = [];
                this.articleChecked = [];
                this.editingList.data.forEach(function (element) {
                  element.checked = false;                  
                  for (let i = 0; i < dataparams.articles.length; i++){
                    if (dataparams.articles[i].article_id === element.article_id){
                        element['addedJM'] = true
                      }
                  }  
                });
                this.modalRef.hide();
          }
        });
    }
  }

  onSizeChange() {
    this.loadEditingData(this.currentPage, "datee", "desc");
  }

  decline(): void {
    this.modalRef.hide();
  }


  doDeleteArticle() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    var articleArr = this.articleChecked.map((o) => {
      return { article_id: o.article_id };
    });

    this.http
      .post<UserResponse>(
        this.parent.url + "user/article/delete",
        articleArr,
        this.options
      )
      .subscribe(
        (result: any) => {
          setTimeout(() => {
            this.loadPromise(this.currentPage, "datee", "desc").then(
              (result) => {
                this.editingList = result;
                if (this.checkedAllModel == true) {
                  this.onCheckedAllChange(this.editingList);
                }
                this.checkAnyChecked();
                this.alerts = [];
                this.alerts.push({
                  type: "success",
                  msg: "Article has been deleted",
                  timeout: 3000,
                });
                this.articleChecked = [];
                this.datachecked = [];
                this.editingList.data.forEach(function (element) {
                  element.checked = false;
                });
                this.deleteShow = false;
                this.modalRef.hide();
                this.parent.progressRef.complete();
              }
            );
          }, 3000);
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000,
          });
          this.articleChecked = [];
          this.datachecked = [];
          this.editingList.data.forEach(function (element) {
            element.checked = false;
          });
          this.deleteShow = false;
          this.modalRef.hide();
          this.parent.progressRef.complete();
        }
      );
  }

  loadPromise(page, field, sort) {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }

      let editingParam = {
        category_set: this.parent.groupCategoryModel,
        category_id:
          this.parent.subCategoryModel == "All Sub Categroy"
            ? "all"
            : this.parent.subCategoryModel,
        user_media_type_id: this.parent.groupMediaModel,
        media_id: this.parent.subMediaModel,
        term: this.searchModel,
        start_date: moment(this.parent.periodFromModel)
          .format("YYYY-MM-DD")
          .toString(),
        end_date: moment(this.parent.periodEndModel)
          .format("YYYY-MM-DD")
          .toString(),
        maxSize: this.pageSize,
        page: page - 1,
        order_by: field,
        order: sort,
      };

      this.http
        .post<UserResponse>(
          this.parent.url + "user/editing/",
          editingParam,
          this.options
        )
        .subscribe(
          (result: any) => {
            result.data.forEach(function (element) {
              element.checked = false;
            });
            resolve(result);
          },
          (err: any) => {
            this.alerts = [];
            this.alerts.push({
              type: "danger",
              msg: "snap, error while loading data.",
              timeout: 3000,
            });
            reject(this.alerts);
          }
        );
    });
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter((alert) => alert !== dismissedAlert);
  }

  loadEditingData(page, field, sort) {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      term: this.searchModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString(),
      maxSize: this.pageSize,
      page: page - 1,
      order_by: field,
      order: sort,
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/editing/",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          result.data.forEach(function (element) {
            element.checked = false;
          });
          this.editingList = result;
          if (this.checkedAllModel == true) {
            this.onCheckedAllChange(this.editingList);
          }
          this.checkAnyChecked();
          this.parent.progressRef.complete();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000,
          });
          this.parent.progressRef.complete();
        }
      );
  }

  onCheckedChange(client: any) {
    if (client.checked == true) {
      this.articleChecked.push({
        article_id: client.article_id,
        category_id: client.category_id,
      });
      this.datachecked.push(client) 
      this.deleteShow = true;
      if (this.trigerclient === 'jm'){
        this.deleteShowsend = true
      }
      
    } else {
      let index = this.articleChecked.findIndex(
        (res) => res.article_id === client.article_id
      );
      this.articleChecked.splice(index, 1);
      let index2 = this.datachecked.findIndex(
        (res) => res.article_id === client.article_id
      );
      this.datachecked.splice(index2, 1);
      if (this.articleChecked.length < 1) {
        this.deleteShow = false;
        if (this.trigerclient === 'jm'){
          this.deleteShowsend = false
        }
      }
    }    
    
  }

  checkAnyChecked() {
    if (this.articleChecked.length > 0) {
      for (let i = 0; i < this.editingList.data.length; i++) {
        for (let j = 0; j < this.articleChecked.length; j++) {
          if (
            this.editingList.data[i].article_id ==
            this.articleChecked[j].article_id
          ) {
            this.editingList.data[i].checked = true;
          }
        }
      }
    }
    if (this.datachecked.length > 0) {
      for (let i = 0; i < this.editingList.data.length; i++) {
        for (let j = 0; j < this.datachecked.length; j++) {
          if (
            this.editingList.data[i].article_id ==
            this.datachecked[j].article_id
          ) {
            this.editingList.data[i].checked = true;
          }
        }
      }
    }
  }

  onCheckedAllChange(data) {
    if (this.checkedAllModel == true) {
      this.editingList.data.forEach(function (element) {
        element.checked = true;
      });
      this.articleChecked = [];
      this.datachecked = [];
      for (let i = 0; i < this.editingList.data.length; i++) {
        this.articleChecked.push({
          article_id: this.editingList.data[i].article_id,
          category_id: this.editingList.data[i].category_id,
        });
        this.datachecked.push(this.editingList.data[i]);
      }
      this.deleteShow = true;
      if (this.trigerclient === 'jm'){
        this.deleteShowsend = true
      }
    } else {
      this.editingList.data.forEach(function (element) {
        element.checked = false;
      });
      this.articleChecked = [];
      this.datachecked = [];
      this.deleteShow = false;
      if (this.trigerclient === 'jm'){
        this.deleteShowsend = false;
      }
    }
  }

  loadKeyWords(categories) {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = { category_id: categories.join() };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/keywords/",
        param,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          var res = [];
          res = result.data;

          return res;
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, failed while loading data.",
            timeout: 3000,
          });
          this.editToneModal.hide();
          this.parent.progressRef.complete();
        }
      );
  }

  showSearchModal(article: any) {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = { article_id: article.article_id };
    this.http
      .post<UserResponse>(
        this.parent.url + "user/keywords-by-article-id/",
        param,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          var res = [];
          res = result.data;
          var argumenta = [];
          var args = res.join().split(",");
          for (let i = 0; i < args.length; i++) {
            var argsExp = args[i].split('" "');
            if (argsExp.length > 1) {
              for (let j = 0; j < argsExp.length; j++) {
                var re = new RegExp('"', "gi");
                var rplc = argsExp[j].replace(re, "");
                argumenta.push(rplc);
              }
            } else {
              var re = new RegExp('"', "gi");
              var rplc = args[i].replace(re, "");
              argumenta.push(rplc);
            }
          }

          highlightedWord = argumenta.join();
          var re = new RegExp(",", "gi");
          var highlightedWord = highlightedWord.replace(re, "||");

          if (article.summary == "" || article.summary == undefined) {
            this.searchModalContainer.contentModel =
              highlightedWord == ""
                ? this.highlighted(article.content, article.category_id)
                : this.highlighted(article.content, highlightedWord);
            this.searchModalContainer.summaryModel =
              highlightedWord == ""
                ? this.highlightedsummary(article.content, article.category_id)
                : this.highlightedsummary(article.content, highlightedWord);
          } else {
            this.searchModalContainer.contentModel =
              highlightedWord == ""
                ? this.highlighted(article.summary, article.category_id)
                : this.highlighted(article.summary, highlightedWord);
            this.searchModalContainer.summaryModel =
              highlightedWord == ""
                ? this.highlightedsummary(article.summary, article.category_id)
                : this.highlightedsummary(article.summary, highlightedWord);
          }
          this.searchModalContainer.filePdfModel = article.file_pdf;
          this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
            "."
          )[1];
          this.searchModalContainer.categoriesList =
            article.categories.length == 0
              ? [article.category_id]
              : article.categories;
          this.searchModalContainer.articleArr = article;
          if (article.title_edit === "" || article.title_edit === undefined) {
            this.searchModalContainer.titleModel = article.title;
          } else {
            this.searchModalContainer.titleModel = article.title_edit;
          }
          this.searchModalContainer.dateModel = article.datee;
          this.searchModalContainer.mediaModel = article.media_name;
          this.searchModalContainer.pageDesc = {
            print: false,
            pdf_text: true,
            doc: true,
            save: true,
            close: true,
            link: true,
          };
          this.searchModalContainer.urlFileModel =
            "https://input.digivla.id/media_tv/" +
            article.file_pdf.split("-")[0] +
            "/" +
            article.file_pdf.split("-")[1] +
            "/" +
            article.file_pdf.split("-")[2] +
            "/" +
            article.file_pdf;
          this.searchModalContainer.searchModal.show();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, failed while loading data.",
            timeout: 3000,
          });
          this.parent.progressRef.complete();
        }
      );
  }

  highlighted(value: any, args: any): any {
    let argsArray = [];
    if (args.includes("&&")) {
      argsArray = args.split("&&");
    } else if (args.includes("||")) {
      argsArray = args.split("||");
    } else {
      argsArray.push(args);
    }
    for (let i = 0; i < argsArray.length; i++) {
      var re = new RegExp(argsArray[i].trimEnd().trimStart(), "gi");
      value = value.replace(
        re,
        "<mark>" + argsArray[i].trimEnd().trimStart() + "</mark>"
      );
    }
    return value;
  }
  highlightedsummary(value: any, args: any): any {
    let argsArray = [];
    if (args.includes("&&")) {
      argsArray = args.split("&&");
    } else if (args.includes("||")) {
      argsArray = args.split("||");
    } else {
      argsArray.push(args);
    }
    return value;
  }
  editTone(article_id, category_id, tone) {
    this.article_idMentionModel = article_id;
    this.category_idMentionModel = category_id;
    this.toneModel = tone;
    this.editToneModal.show();
  }

  doUpdateTone() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_id: this.category_idMentionModel,
      article_id: this.article_idMentionModel,
      tone: this.toneModel,
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/tone/update",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          for (let i = 0; i < this.editingList.data.length; i++) {
            if (this.editingList.data[i].article_id == result.data.article_id) {
              this.editingList.data[i].tone = result.data.tone;
              this.editingList.data[i].flags = result.data.flags;
              this.editingList.data[i].advalue_fc = result.data.advalue_fc;
              this.editingList.data[i].advalue_bw = result.data.advalue_bw;
            }
          }

          this.alerts = [];
          this.alerts.push({
            type: "success",
            msg: "Tone has been edited",
            timeout: 3000,
          });
          this.parent.progressRef.complete();
          this.editToneModal.hide();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, Tone failed to update.",
            timeout: 3000,
          });
          this.editToneModal.hide();
          this.parent.progressRef.complete();
        }
      );
  }

  editTitle(article_title, article_id, category_id) {
    this.editname = "Edit Title";
    this.summaryModel = article_title;
    this.getarticleidi = article_id;
    this.getarticleccategory = category_id;
    this.editSummaryModal.show();
  }
  editSummary(article_id, category_id, summary, content) {
    this.editname = "Edit Summary";
    this.article_idMentionModel = article_id;
    this.category_idMentionModel = category_id;
    if (summary == "" || summary == undefined) {
      this.summaryModel = content;
    } else {
      this.summaryModel = summary;
    }
    this.editSummaryModal.show();
  }
  doUpdateTitle() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let editingParam = {
      category_id: this.getarticleccategory,
      article_id: this.getarticleidi,
      title: this.summaryModel,
    };
    this.http
      .post<UserResponse>(
        this.parent.url + "user/title/update",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          for (let i = 0; i < this.editingList.data.length; i++) {
            if (this.editingList.data[i].article_id == result.data.article_id) {
              this.editingList.data[i].flags = result.data.flags;
              this.editingList.data[i].title_edit = result.data.title_edited;
            }
          }
          this.alerts = [];
          this.alerts.push({
            type: "success",
            msg: "Tone has been edited",
            timeout: 3000,
          });
          this.parent.progressRef.complete();
          this.editSummaryModal.hide();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, Tone failed to update.",
            timeout: 3000,
          });
          this.editSummaryModal.hide();
          this.parent.progressRef.complete();
        }
      );
  }
  doUpdateSummary() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_id: this.category_idMentionModel,
      article_id: this.article_idMentionModel,
      summary: this.summaryModel,
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/summary/update",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          for (let i = 0; i < this.editingList.data.length; i++) {
            if (this.editingList.data[i].article_id == result.data.article_id) {
              this.editingList.data[i].summary = result.data.summary;
              this.editingList.data[i].flags = result.data.flags;
            }
          }
          this.alerts = [];
          this.alerts.push({
            type: "success",
            msg: "Tone has been edited",
            timeout: 3000,
          });
          this.parent.progressRef.complete();
          this.editSummaryModal.hide();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, Tone failed to update.",
            timeout: 3000,
          });
          this.editSummaryModal.hide();
          this.parent.progressRef.complete();
        }
      );
  }

  clickSearch() {
    this.currentPage = 1;
    this.loadEditingData(1, "datee", "desc");
  }

  flagLevelmenu() {
    this.flagToneSumary = this.parent.menuLevel;
  }

  ngOnInit() {
    this.articleChecked = [];
    this.datachecked = [];
    this.loadEditingData(1, "datee", "desc");
    this.subscription = this.childListener.notifyObservable$.subscribe(
      (res) => {
        if (res.hasOwnProperty("option") && res.option === "call_child") {
          this.loadEditingData(1, "datee", "desc");
        } else if (
          res.hasOwnProperty("option") &&
          res.option === "save_article"
        ) {
          for (let i = 0; i < this.editingList.data.length; i++) {
            if (this.editingList.data[i].article_id == res.value.article_id) {
              this.editingList.data[i].categories = res.value.categories;
            }
          }
        }
      }
    );

    this.flagLevelmenu();
    if (localStorage.getItem("client") === "tkgaa") {
      this.getGroupCategory();
      this.trigerclient = 'jm'
      this.autocomplete = [];
      interface UserResponse {
        data: Object;
      }
      this.http
        .post<UserResponse>(
          this.parent.url + "topic/get-topic-list/",
          "",
          this.options
        )
        .subscribe(
          (result: any) => {
            result.data.forEach((element, index) => {
              let dataobj = {
                id: index + 1,
                name: element,
              };
              this.autocomplete.push(dataobj);
            });
          },
          (err: any) => {}
        );
      this.showpanelnew = true;
    } else {
      this.trigerclient = 'other'
      this.showpanelnew = false;
    }
  }
  modaltopic(data) {
    this.dataalltopicgetinit = data;
    this.subTopicDetail.show();
    this.getdatatopic();
  }
  getdatatopic() {
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      article_id: this.dataalltopicgetinit.article_id,
    };
    this.http
      .post<UserResponse>(
        this.parent.url + "topic/get/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.topicDetailsList = [];
          result.data.topic.forEach((element) => {
            this.topicDetailsList.push(element);
          });
          for (let i = 0; i < this.editingList.data.length; i++) {
            if (this.editingList.data[i].article_id == result.data.article_id) {
              this.editingList.data[i].topic = result.data.topic;
            }
          }
        },
        (err: any) => {}
      );
  }
  editTopicsubmit() {
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      article_id: this.dataalltopicgetinit.article_id,
      topic: this.oldtopic,
      new_topic: this.topicModel,
    };
    this.http
      .post<UserResponse>(
        this.parent.url + "topic/update/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.getdatatopic();
          this.topicModals.hide();
        },
        (err: any) => {}
      );
  }
  addTopic() {
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      article_id: this.dataalltopicgetinit.article_id,
      date: this.dataalltopicgetinit.datee,
      topic:
        this.topicNameModel.name != undefined
          ? this.topicNameModel.name
          : this.topicNameModel,
      media_id: this.dataalltopicgetinit.media_id,
      category_id: this.dataalltopicgetinit.category_id,
      title: this.dataalltopicgetinit.title,
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "topic/add/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.getdatatopic();
        },
        (err: any) => {}
      );
  }
  editTopic(details) {
    this.topicModals.show();
    this.topicModel = details;
    this.oldtopic = details;
  }
  deleteTopic(details) {
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      article_id: this.dataalltopicgetinit.article_id,
      topic: details,
    };
    this.http
      .post<UserResponse>(
        this.parent.url + "topic/delete/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.getdatatopic();
        },
        (err: any) => {}
      );
  }

  ngOnDestroy() {
    this.parent.progressRef.complete();
    this.subscription.unsubscribe();
  }
}
