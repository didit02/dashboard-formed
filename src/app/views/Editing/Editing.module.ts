import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { EditingComponent } from './Editing.component';
import { EditingRoutingModule } from './Editing-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';

import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'
import { ModalModule } from 'ngx-bootstrap/modal';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  imports: [
    CommonModule,
    // BrowserModule,
    EditingRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    ArticleDetailsModule,
    AutocompleteLibModule
  ],
  declarations: [
    EditingComponent,    
    // ArticleDetailsComponent,
    // SaveArticlesComponent
]
})
export class EditingModule { }
