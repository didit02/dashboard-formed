import { Component, OnInit,ViewChild,Inject,TemplateRef } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgProgress } from '@ngx-progressbar/core'
import {environment} from '../../../environments/environment'
import { DefaultLayoutComponent } from '../../containers/default-layout/default-layout.component'
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-GroupCategory',
  templateUrl: './GroupCategory.component.html',
  styleUrls: ['./GroupCategory.component.scss']
})
export class GroupCategoryComponent implements OnInit {
  url=environment.apiUrl;
  alerts;
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };
  newGroupCategory;
  checkedAllModel;
  pointerArray;

  editGroupCategory;

  groupCategoryList=[];
  groupCategoryDetailList=[];

  modalRef: BsModalRef;

  @ViewChild('groupCategoryDetail') public groupCategoryDetail;
  @ViewChild('editNameGroupCategory') public editNameGroupCategory;

  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,private http: HttpClient,public progress: NgProgress,private childListener: ChildListenerService,private router: Router,private modalService: BsModalService) { }
  
  doUpdateGroupCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = {"category_set": this.pointerArray.category_set, "group_name":this.editGroupCategory}
    this.http.post<UserResponse>(this.url + 'user/update/group-category',param,this.options)
    .subscribe((result: any) => {
      for (let i=0;i<this.groupCategoryList.length;i++){
        if (this.groupCategoryList[i].category_set== result.data.category_set){
          this.groupCategoryList[i].descriptionz = result.data.descriptionz;
        }
      }
      this.editNameGroupCategory.hide();
      this.editGroupCategory ='';
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Media has been updated','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.editNameGroupCategory.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while saving data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  updateCategory(client){
    this.pointerArray = client;
    this.editGroupCategory = client.descriptionz;
    this.editNameGroupCategory.show();
  }

  doDeleteGroupCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = {"category_set": this.pointerArray.category_set}
    this.http.post<UserResponse>(this.url + 'user/delete/group-category',param,this.options)
    .subscribe((result: any) => {
      for (let i=0;i<this.groupCategoryList.length;i++){
        if (this.groupCategoryList[i].category_set== result.data.category_set){
          this.groupCategoryList.splice(i,1);
        }
      }
      this.modalRef.hide(); 
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Media has been deleted','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.modalRef.hide(); 
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  loadGroupCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    
    this.http.get<UserResponse>(this.url + 'user/categories/',this.options)
      .subscribe((result: any) => {        
        this.parent.progressRef.complete();
        this.groupCategoryList =  result.results;
      },(err:any)=>{
        this.alerts=[];
        this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
        this.parent.progressRef.complete();
      });   
  }

  showDetails(client){
    this.parent.progressRef.start();
    this.pointerArray = client;
    
    interface UserResponse {
      data: Object;
    }
    let param = {"category_set": client.category_set}
    this.http.post<UserResponse>(this.url + 'user/subcategory-chosen',param,this.options)
    .subscribe((result: any) => {
      this.groupCategoryDetailList =[];
      this.groupCategoryDetailList = result.data;    
      
      this.parent.progressRef.complete();
      this.groupCategoryDetail.show(); 
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    });   
           
  }

  addNewGroupCategory(){
    this.parent.progressRef.start();   
    interface UserResponse {
      data: Object;
    }
    let param = {"group_name": this.newGroupCategory}
    this.http.post<UserResponse>(this.url + 'user/create/group-category',param,this.options)
    .subscribe((result: any) => {
      this.groupCategoryList.push(result.data);
      this.newGroupCategory ='';
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Category has been created','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  openModal(template: TemplateRef<any>,client) {
    this.pointerArray = client;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  decline(): void {
    this.modalRef.hide();
  }

  doSaveSubGroupCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    var param = {
      'category_set' : this.pointerArray.category_set,
      'category_list' : this.groupCategoryDetailList
    };
     

    this.http.post<UserResponse>(this.url + 'user/group-sub-category/update',param,this.options)
    .subscribe((result: any) => {
      this.groupCategoryDetail.hide();
      this.groupCategoryDetailList =[];
      this.alerts=[];
      this.alerts.push({'type':'success','msg':' Sub Group Category has been updated','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.groupCategoryDetail.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while saving data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  onCheckedAllChange(){
    if (this.checkedAllModel==true){      
      this.groupCategoryDetailList.forEach(function(element) { element.chosen=true });
    } else{
      this.groupCategoryDetailList.forEach(function(element) { element.chosen=false }); 
    }
  }
  
  ngOnInit() {
    this.loadGroupCategory();
  }

}
