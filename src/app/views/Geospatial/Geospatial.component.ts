import { Component, OnInit, OnDestroy, ViewChild, Inject } from "@angular/core";
import {MapChart } from "angular-highcharts";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { ListenerService } from "../../views/Services/listener.service";
@Component({
  selector: "app-Geospatial",
  templateUrl: "./Geospatial.component.html",
  styleUrls: ["./Geospatial.component.scss"],
})
export class GeospatialComponent implements OnInit {
  mapChart: any;
  customSeries: any;
  seriescolor: any;
  mincolor: string = '#D3D3D3';
  maxcolor: string = '#BFFFB2';
  optionsmap:any;
  geoname:string;
  currentPagemaps :number = 1;
  articlesList: any = [];
  pageSize: number = 10;
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token")
  });
  options = { headers: this.headers };
  alerts:any;
  @ViewChild("showArticlesModalmaps") public showArticlesModalmaps;
  @ViewChild("searchModalContainer") public searchModalContainer;
  constructor(
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
    private http: HttpClient,
    private statelocal: ListenerService,
  ) {
  }

  ngOnInit() {
    this.renderdatachart();
  }
  pageChangedmaps(event){
    this.currentPagemaps = event.page;
    this.loaddatalistmaps();
  }
  renderdatachart() {
    this.statelocal.getlocalgeojson().subscribe(data => {
      interface UserResponse {
        data: Object;
      }
      let dataallloc = [];
      let body = {
        start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
        end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString()
      }
      this.http.post<UserResponse>("https://kemlu.digivla.id/apidigivla/api/v1/all-count/", body , this.options)
        .subscribe(
          (result: any) => {
            result.data.forEach(element => {
              dataallloc.push([element.value,element.key.split('KABUPATEN ')[1]])
            });
            let interval = 0;
            this.customSeries = []
            dataallloc.forEach((item) => {
              if (item[0] > 0) {
                this.seriescolor = this.calculateDark(this.maxcolor, interval);
              } else {
                this.seriescolor = this.mincolor;
              }
              this.customSeries.push({
                name: item[1],
                percent: item[0],
                color: this.seriescolor,
                data: [{ 'value': item[0], 'name': item[1] }],
                borderColor: '#212121',
                states: {
                  hover: {
                    color: '#52DE97',
                  },
                },
              })
              interval += 0.2;
            })

            this.optionsmap = {
              chart: {
                backgroundColor: 'rgba(255, 255, 255, 0.0)',
                height: 300
              },
              title: {
                text: ''
              },
              mapNavigation: {
                enabled: true
            },
              legend: {
                enabled: false,
                symbolRadius: 0,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 0,
                itemStyle: {
                  color: '#c9c9ca',
                },
                itemHoverStyle: {
                  color: '#FFFFFF',
                },
                itemHiddenStyle: {
                  color: '#414144',
                },
                useHTML: true,
                navigation: {
                  activeColor: '#cccccc',
                  inactiveColor: '#737373',
                  style: {
                    color: '#4a4a4a',
                    fontSize: '15px',
                  },
                  buttonOptions: {
                    enabled: false
                    }
                },
              },
              plotOptions: {
                map: {
                  allAreas: false,
                  joinBy: ['name'],
                  mapData: data,
                  tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.value}</b>'
                  }

                },
                series:{
                  point:{
                      events:{
                        click: (event)=>{
                        	this.onclickmaps(event);
                        }
                        }
                    }
                }
              },
              credits: {
                enabled: false
              },
              series: this.customSeries
            }
            this.mapChart = new MapChart(this.optionsmap); 
          },
          err => { 
          }
        );
    })
  }
  LightenDarkenColor(col, amt) {
    this.calculateDark(col, amt)
  }
  calculateDark(col, amt) {
    var usePound = false;
    if (col[0] == '#') {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) - amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 0;
    var b = (num >> 8) & 0x00ff;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;
    var g = num & 0x0000ff;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  }
  onclickmaps(event){
    this.geoname = event.point.name;
    interface UserResponse {
      data: Object;
    }
    let params = {
      start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      page: 0,
      max_size: 10,
      geo_loc: this.geoname
    };
    this.http
      .post<UserResponse>(
        'https://kemlu.digivla.id/apidigivla/api/v1/article-by-geo/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModalmaps.show();
        },
        (err: any) => {
        }
      );
  }
  showSearchModalmaps(article){
    interface UserResponse {
      data: Object;
    }
    let params = {
      article_id:article.article_id
    };
    this.http
      .post<UserResponse>(
        'https://kemlu.digivla.id/apidigivla/api/v1/article-by-id/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          var res = [];
          res = result.data;
          var argumenta = [];
          var args = res.join().split(",");
          for (let i = 0; i < args.length; i++) {
            var argsExp = args[i].split('" "');
            if (argsExp.length > 1) {
              for (let j = 0; j < argsExp.length; j++) {
                var re = new RegExp('"', "gi");
                var rplc = argsExp[j].replace(re, "");
                argumenta.push(rplc);
              }
            } else {
              var re = new RegExp('"', "gi");
              var rplc = args[i].replace(re, "");
              argumenta.push(rplc);
            }
          }
          highlightedWord = argumenta.join();
          var re = new RegExp(",", "gi");
          var highlightedWord = highlightedWord.replace(re, "||");

          this.searchModalContainer.contentModel =
          highlightedWord == ""
          ? this.highlightedsummary(article.content, article.category_id)
          : this.highlightedsummary(article.content, highlightedWord);
      this.searchModalContainer.filePdfModel = article.file_pdf;
      this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
        "."
      )[1];
          this.searchModalContainer.summaryModel =
            highlightedWord == ""
              ? this.highlightedsummary(article.content, article.category_id)
              : this.highlightedsummary(article.content, highlightedWord);
          this.searchModalContainer.filePdfModel = article.file_pdf;
          this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
            "."
          )[1];
          this.searchModalContainer.categoriesList = [];
          this.searchModalContainer.articleArr = article;
          if (article.title_edit === "" || article.title_edit === undefined) {
            this.searchModalContainer.titleModel = article.title;
          } else {
            this.searchModalContainer.titleModel = article.title_edit;
          }
          this.searchModalContainer.dateModel = article.datee;
          this.searchModalContainer.mediaModel = article.media_name;
          this.searchModalContainer.pageDesc = {
            print: true,
            pdf_text: false,
            doc: true,
            save: false,
            close: true,
            scan_media: true,
            text_print: true,
            link: true
          };
          this.searchModalContainer.urlFileModel =
            "https://input.digivla.id/media_tv/" +
            article.file_pdf.split("-")[0] +
            "/" +
            article.file_pdf.split("-")[1] +
            "/" +
            article.file_pdf.split("-")[2] +
            "/" +
            article.file_pdf;
          this.searchModalContainer.searchModal.show();
        },
        (err: any) => {
        }
      );
}
  loaddatalistmaps(){
    interface UserResponse {
      data: Object;
    }
    let params = {
      start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      page: this.currentPagemaps - 1,
      max_size: 10,
      geo_loc: this.geoname
    };
    this.http
      .post<UserResponse>(
        'https://kemlu.digivla.id/apidigivla/api/v1/article-by-geo/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = [];
          this.articlesList = result;
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );

  }
  highlightedsummary(value: any, args: any): any {
    // let argsArray = args.split('&&')
    let argsArray = [];
    let keyAray = [];

    if (args.includes("&&")) {
      argsArray = args.split("&&");
    } else if (args.includes("||")) {
      argsArray = args.split("||");
    } else {
      argsArray.push(args);
    }
    for (let i = 0; i < argsArray.length; i++) {
      var re = new RegExp(argsArray[i].trimEnd().trimStart(), "gi");
    }
    return value;
  }
}
