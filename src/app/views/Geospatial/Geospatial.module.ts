import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as highmaps from 'highcharts/modules/map.src';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

import { GeospatialRoutingModule } from "./Geospatial-routing.module";
import { GeospatialComponent } from "./Geospatial.component";
@NgModule({
  imports: [
    GeospatialRoutingModule, 
    FormsModule,
    CommonModule,
    ChartsModule, 
    ChartModule, 
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ArticleDetailsModule, 
    AlertModule
  ],
  declarations: [GeospatialComponent],
  providers: [
    { provide: HIGHCHARTS_MODULES, useFactory: () => [highmaps] }
  ]
})
export class GeospatialModule { }
