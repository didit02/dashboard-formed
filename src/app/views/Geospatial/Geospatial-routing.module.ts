import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GeospatialComponent } from "./Geospatial.component";

const routes: Routes = [
  {
    path: "",
    component: GeospatialComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeospatialRoutingModule {}
