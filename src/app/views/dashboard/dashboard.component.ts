import { Component, OnInit, OnDestroy, ViewChild, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { AlertComponent } from "ngx-bootstrap/alert/alert.component";
import { Subscription } from "rxjs/Subscription";
import { ChildListenerService } from "../../views/Services/ChildListener.service";
import { ListenerService } from "../../views/Services/listener.service";
import { environment } from "../../../environments/environment";
import { Chart, MapChart } from "angular-highcharts";
import { _localeFactory } from "@angular/core/src/application_module";
import { truncateWithEllipsis } from "@amcharts/amcharts4/.internal/core/utils/Utils";
@Component({
  templateUrl: "dashboard.component.html",
  styleUrls: ["dashboard.component.scss"]
})
export class DashboardComponent implements OnInit, OnDestroy {
  constructor(
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
    private http: HttpClient,
    private childListener: ChildListenerService,
    private statelocal: ListenerService,
  ) {}
  @ViewChild("showArticlesModal") public showArticlesModal;
  @ViewChild("showArticlesModalmaps") public showArticlesModalmaps;
  @ViewChild("showArticlesModaltopic") public showArticlesModaltopic;
  @ViewChild("searchModalContainer") public searchModalContainer;
  private subscription: Subscription;
  isDataAvailable: boolean = false;
  isDataBAvailable: boolean = false;
  menuLevel: any;
  toneclick:boolean = false;
  url: any = environment.apiUrl;
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token")
  });
  options = { headers: this.headers };
  mapChart: any;
  datanewstrend = [];
  mapCharthidden: any;
  alerts:any;
  myInterval:any = false;
  chart:any;
  chartArray = [];
  optionsmap:any;
  articlesList: any = [];
  pageSize: number = 10;
  currentPage: number = 1;
  currentPagemaps :number = 1;
  currentPagetopic :number = 1;
  dateAxis:any;
  percentagepie:any;
  ToneChart = false;
  tone:any;
  media_id:any;
  media_name:any;
  subCategory:any;
  customSeries: any;
  seriescolor: any;
  mincolor: string = '#D3D3D3';
  maxcolor: string = '#BFFFB2';
  groupMediaOption: any[] = [];
  subMediaOption: any[] = [];
  geoname:any;
  mediaVisibilityhidden = new Chart({});
  summaryPiehidden = new Chart({});
  CoverageTonalityhidden = new Chart({});
  toneByMediahidden = new Chart({});
  toneByCategoryhidden = new Chart({});
  ewsHidden = new Chart({});
  mediaRawData:any;
  mediaVisibilityData = [];
  mediaVisibilityXAxis = [];
  mediaVisibilityChart = new Chart({});
  mediaVisibilityFlag = false;
  summaryPieChart:any;
  pieChartData = [];
  piehiddenTone:any;
  topicgetclick :any;
  trendingHighlightList = [];
  toneByCategoryChart = new Chart({});
  public toneByCategoryLabels: string[] = [];
  public toneByCategoryData: any[] = [{ data: [], label: "" }];
  CoverageTonalityChart:any;
  public barChartCoverageTonalityLabels: string[] = [];
  public barChartCoverageTonalityData: any[] = [{ data: [], label: "" }];
  toneByMedia = new Chart({});
  public TonebyMediaSelectionLabels: string[] = [];
  public TonebyMediaSelectionyData: any[] = [{ data: [], label: "" }];
  EWSData = [];
  EWSXAxis = [];
  EWSChart = new Chart({});
  EWSFlag = false;
  showpanelnew = false;
  wordcloud = {};
  wordcloudhidden = {};
  showpietone:boolean = false;
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  loadMediaVisibility() {
    this.mediaVisibilityChart.ref$.subscribe((res: any) => {
      res.showLoading();
    });
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };
    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/media-visibility",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          let dataGraph = [];
          let xAxisGraph = [];
          let pieChartlabel = [];
          let pieChartDataArr = [];

          for (let i = 0; i < result.data.length; i++) {
            let dataSubCategory = [];
            for (
              let j = 0;
              j < result.data[i].category_id_per_day.buckets.length;
              j++
            ) {
              dataSubCategory.push(
                result.data[i].category_id_per_day.buckets[j].doc_count
              );
            }
            dataGraph.push({
              name: result.data[i].key,
              data: dataSubCategory,
              type: "spline"
            });
            pieChartDataArr.push({
              name: result.data[i].key,
              y: result.data[i].doc_count
            });
          }
          for (
            let i = 0;
            i < result.data[0].category_id_per_day.buckets.length;
            i++
          ) {
            xAxisGraph.push(
              result.data[0].category_id_per_day.buckets[i].key_as_string
            );
          }

          this.mediaVisibilityData = [];
          this.mediaVisibilityData = dataGraph;
          this.mediaVisibilityXAxis = [];
          this.mediaVisibilityXAxis = xAxisGraph;
          this.mediaVisibilityLoader();
          this.pieChartData = [{ name: "Category", data: pieChartDataArr }];
          this.summaryPieChartLoader();
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  loadTonality() {
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tones",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          let barChartCoverageTones = [];
          let xAxisGraphBar = [];
          let datauseloop;
          let datachartpie = [];
          if(localStorage.getItem('client') === 'tkgaa'){
            datauseloop = result.data.chart_bar
            for (let i = 0; i < result.data.chart_pie.length; i++) {
              datachartpie.push({
                name: result.data.chart_pie[i].sentiment == 1 ? "Positive": result.data.chart_pie[i].sentiment == 0 ? "Neutral" : "Negative",
                y: result.data.chart_pie[i].value
              });
            }
            const datapieuse = [{ name: "Category", data: datachartpie }];
            this.renderpercentagepie(datapieuse)
            this.showpietone = true;
          }else{
            datauseloop = result.data
            this.showpietone = false;
          }
          for (let i = 0; i < datauseloop.length; i++) {
            let dataSubCategory = [];
            for (
              let j = 0;
              j < datauseloop[i].tone_per_day.buckets.length;
              j++
            ) {
              dataSubCategory.push(
                datauseloop[i].tone_per_day.buckets[j].doc_count
              );
            }
            var index =
            datauseloop[i].key == 1 ? 0 : datauseloop[i].key == 0 ? 1 : 2;
            barChartCoverageTones.splice(index, 0, {
              data: dataSubCategory,
              name:
              datauseloop[i].key == 1
                  ? "Positive"
                  : datauseloop[i].key == 0
                  ? "Neutral"
                  : "Negative"
            });
          }

          for (let i = 0; i < datauseloop[0].tone_per_day.buckets.length; i++) {
            xAxisGraphBar.push(
              datauseloop[0].tone_per_day.buckets[i].key_as_string
            );
          }

          this.barChartCoverageTonalityData = [];
          this.barChartCoverageTonalityData = barChartCoverageTones;
          this.barChartCoverageTonalityLabels = [];
          this.barChartCoverageTonalityLabels = xAxisGraphBar;

          this.coverageTonalityLoader();
          this.parent.progressRef.complete();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  loadTrendingHighlights() {
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/high-lights",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.trendingHighlightList = result.data;
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  loadToneByCategory() {
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tone-by-category",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          let barChartCoverageTones = [];
          let positiveData = [];
          let neutralData = [];
          let negativeData = [];
          let xAxisGraphBar = [];
          for (let i = 0; i < result.data.length; i++) {
            for (let j = 0; j < result.data[i].tones.length; j++) {
              if (result.data[i].tones[j].positive != undefined) {
                positiveData.push(result.data[i].tones[j].positive);
              } else if (result.data[i].tones[j].neutral != undefined) {
                neutralData.push(result.data[i].tones[j].neutral);
              } else {
                negativeData.push(result.data[i].tones[j].negative);
              }
            }
          }
          barChartCoverageTones = [
            {
              data: positiveData,
              name: "Positive"
            },
            {
              data: neutralData,
              name: "Neutral"
            },
            {
              data: negativeData,
              name: "Negative"
            }
          ];

          for (let i = 0; i < result.data.length; i++) {
            xAxisGraphBar.push(result.data[i].category_id);
          }

          this.toneByCategoryData = [];
          this.toneByCategoryData = barChartCoverageTones;
          this.toneByCategoryLabels = [];
          this.toneByCategoryLabels = xAxisGraphBar;

          this.toneByCategoryLoadChart();
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  loadToneByMedia() {
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tone-by-media",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.mediaRawData = result;
          let tonebyMedia = [];
          let positiveData = [];
          let neutralData = [];
          let negativeData = [];
          let xAxisGraphBar = [];
          for (let i = 0; i < result.data.length; i++) {
            for (let j = 0; j < result.data[i].tones.length; j++) {
              if (result.data[i].tones[j].positive != undefined) {
                positiveData.push(result.data[i].tones[j].positive);
              } else if (result.data[i].tones[j].neutral != undefined) {
                neutralData.push(result.data[i].tones[j].neutral);
              } else {
                negativeData.push(result.data[i].tones[j].negative);
              }
            }
          }
          tonebyMedia = [
            {
              data: positiveData,
              name: "Positive"
            },
            {
              data: neutralData,
              name: "Neutral"
            },
            {
              data: negativeData,
              name: "Negative"
            }
          ];

          for (let i = 0; i < result.data.length; i++) {
            xAxisGraphBar.push(result.data[i].media_name);
          }

          this.TonebyMediaSelectionyData = [];
          this.TonebyMediaSelectionyData = tonebyMedia;
          this.TonebyMediaSelectionLabels = [];
          this.TonebyMediaSelectionLabels = xAxisGraphBar;
          this.toneByMediaLoadChart();
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  loadEWS() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tone-by-media-tier",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          var xAxisChart = [];
          var chartData = [];
          for (let i = 0; i < result.data.length; i++) {
            for (let j = 0; j < result.data[i].dates.length; j++) {
              var a = xAxisChart.indexOf(result.data[i].dates[j].date);
              if (a < 0) {
                xAxisChart.push(result.data[i].dates[j].date);
              }
            }
          }

          var category_id = [];
          for (let i = 0; i < result.data.length; i++) {
            var datesArr = [];
            for (let j = 0; j < result.data[i].dates.length; j++) {
              var positive = 0;
              var negative = 0;
              for (let k = 0; k < result.data[i].dates[j].tones.length; k++) {
                if (result.data[i].dates[j].tones[k].tonality > 0) {
                  positive = result.data[i].dates[j].tones[k].summary_by_tier;
                } else if (result.data[i].dates[j].tones[k].tonality < 0) {
                  negative = result.data[i].dates[j].tones[k].summary_by_tier;
                }
              }
              var val = positive - negative;
              if (val < 0) {
                val = 0 - val;
                if (val > 15) {
                  val = 15;
                }
              } else if (negative > 0) {
                val = 1;
              } else {
                val = 0;
              }
              datesArr.push(val);
            }
            category_id.push({
              name: result.data[i].category_id,
              data: datesArr,
              type: "spline"
            });
          }

          this.EWSData = [];
          this.EWSData = category_id;

          this.EWSXAxis = [];
          this.EWSXAxis = xAxisChart;

          this.EWSLoader();

          this.parent.progressRef.complete();
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  mediaVisibilityLoader() {
    this.parent.progressRef.start();
    this.mediaVisibilityChart = new Chart({
      chart: {
        type: "line"
      },
      legend: {
        enabled: true,
        maxHeight: 60
      },
      credits: {
        enabled: false
      },
      title: {
        text: "Media Visibility"
      },
      yAxis: {
        title: null
      },
      xAxis: {
        tickInterval: 1,
        categories: this.mediaVisibilityXAxis,
        startOnTick: true,
        minPadding: 0
      },
      plotOptions: {
        series: {
          cursor: "pointer",
          allowPointSelect: true,
          point: {
            events: {
              select: this.onPointSelect.bind(this)
            }
          }
        }
      },
      series: this.mediaVisibilityData,
      exporting: {
        enabled: false
      }
    });

    this.mediaVisibilityhidden = new Chart({
      chart: {
        type: "line",
        height: 600,
        width: 2000
      },
      legend: {
        enabled: true,
        maxHeight: 1000
      },
      credits: {
        enabled: false
      },
      title: {
        text: "Media Visibility"
      },
      yAxis: {
        title: null
      },
      xAxis: {
        tickInterval: 1,
        categories: this.mediaVisibilityXAxis,
        startOnTick: true,
        minPadding: 0
      },
      series: this.mediaVisibilityData,
      exporting: {
        enabled: false
      }
    });
    this.parent.progressRef.complete();
  }

  renderpercentagepie(datapieuse){
    
    this.percentagepie = new Chart({
      chart: {
        type: "pie"
      },
      title: {
        text: "Persentasi Tonality"
      },
      credits: {
        enabled: false
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y:.0f}</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false
          },
          showInLegend: true,
          point: {
            events: {
              select: this.onPieSelectpercentage.bind(this)
            }
          }
        }
      },
      legend: {
        maxHeight: 80
      },
      series: datapieuse,
      exporting: {
        enabled: false
      }
    });
    this.piehiddenTone = new Chart({
      chart: {
        type: "pie"
      },
      title: {
        text: "Persentasi Tonality"
      },
      credits: {
        enabled: false
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y:.0f}</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false
          },
          showInLegend: true,
          point: {
            events: {
              // select: this.onPieSelect.bind(this)
            }
          }
        }
      },
      legend: {
        maxHeight: 80
      },
      series: datapieuse,
      exporting: {
        enabled: false
      }
    });
  }
  summaryPieChartLoader() {
    this.parent.progressRef.start();
    this.summaryPieChart = new Chart({
      chart: {
        type: "pie"
      },
      title: {
        text: "Summary"
      },
      credits: {
        enabled: false
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y:.0f}</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false
          },
          showInLegend: true,
          point: {
            events: {
              select: this.onPieSelect.bind(this)
            }
          }
        }
      },
      legend: {
        maxHeight: 80
      },
      series: this.pieChartData,
      exporting: {
        enabled: false
      }
    });

    this.summaryPiehidden = new Chart({
      chart: {
        type: "pie",
        height: 500,
        width: 500
      },
      title: {
        text: "Summary"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %"
          }
        }
      },
      legend: {
        enabled: false
      },
      series: this.pieChartData,
      exporting: {
        enabled: false
      }
    });
    this.parent.progressRef.complete();
  }

  coverageTonalityLoader() {
    this.parent.progressRef.start();
    this.CoverageTonalityChart = new Chart({
      chart: {
        type: "column"
      },
      title: {
        text: "Coverage Tonality"
      },
      xAxis: {
        categories: this.barChartCoverageTonalityLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        },
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        headerFormat: "<b>{point.x}</b><br/>",
        pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
      },
      plotOptions: {
        column: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          },
          allowPointSelect: true,
          point: {
            events: {
              select: this.onCoverageSelected.bind(this)
            }
          }
        }
      },
      series: this.barChartCoverageTonalityData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });

    this.CoverageTonalityhidden = new Chart({
      chart: {
        type: "column"
      },
      title: {
        text: "Coverage Tonality"
      },
      xAxis: {
        categories: this.barChartCoverageTonalityLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        },
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        headerFormat: "<b>{point.x}</b><br/>",
        pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
      },
      plotOptions: {
        column: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          }
        }
      },
      series: this.barChartCoverageTonalityData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });
    this.parent.progressRef.complete();
  }

  toneByCategoryLoadChart() {
    this.parent.progressRef.start();
    this.toneByCategoryChart = new Chart({
      chart: {
        type: "column"
      },
      title: {
        text: null
      },
      xAxis: {
        categories: this.toneByCategoryLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        },
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        headerFormat: "<b>{point.x}</b><br/>",
        pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
      },
      plotOptions: {
        column: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          },
          allowPointSelect: true,
          point: {
            events: {
              select: this.onCategorySelected.bind(this)
            }
          }
        }
      },
      series: this.toneByCategoryData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });

    this.toneByCategoryhidden = new Chart({
      chart: {
        type: "column"
      },
      title: {
        text: "Tone by Category"
      },
      xAxis: {
        categories: this.toneByCategoryLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        },
        stackLabels: {
          enabled: true
        }
      },
      plotOptions: {
        column: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          }
        }
      },
      series: this.toneByCategoryData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });

    this.parent.progressRef.complete();
  }

  toneByMediaLoadChart() {
    this.parent.progressRef.start();
    this.toneByMedia = new Chart({
      chart: {
        type: "bar"
      },
      title: {
        text: null
      },
      xAxis: {
        categories: this.TonebyMediaSelectionLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: "Tone by Media Selections"
        },
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        headerFormat: "<b>{point.x}</b><br/>",
        pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
      },
      plotOptions: {
        series: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          },
          allowPointSelect: true,
          point: {
            events: {
              select: this.onMediaSelection.bind(this)
            }
          }
        }
      },
      series: this.TonebyMediaSelectionyData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });

    this.toneByMediahidden = new Chart({
      chart: {
        type: "bar"
      },
      title: {
        text: "Tone by Media Selections"
      },
      xAxis: {
        categories: this.TonebyMediaSelectionLabels
      },
      yAxis: {
        min: 0,
        title: {
          text: "Tone by Media Selections"
        },
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        headerFormat: "<b>{point.x}</b><br/>",
        pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
      },
      plotOptions: {
        series: {
          stacking: "normal",
          dataLabels: {
            enabled: true
          }
        }
      },
      series: this.TonebyMediaSelectionyData,
      colors: ["#247332", "#3F84F7", "#992320"],
      exporting: {
        enabled: false
      }
    });
    this.parent.progressRef.complete();
  }

  EWSLoader() {
    this.parent.progressRef.start();
    var yAxisLabels = {
      0: "Potential",
      5: "Emerging",
      10: "Current",
      15: "Crisis"
    };
    console.log(this.EWSData , '====');
    
    this.EWSChart = new Chart({
      chart: {
        type: "line"
      },
      legend: {
        enabled: true,
        maxHeight: 60
      },
      credits: {
        enabled: false
      },
      title: {
        text: "Early Warning System"
      },
      yAxis: {
        max: 15,
        min: 0,
        title: { text: null },
        labels: {
          formatter: function() {
            var value = yAxisLabels[this.value];
            return value !== "undefined" ? value : this.value;
          }
        }
      },
      xAxis: {
        tickInterval: 1,
        categories: this.EWSXAxis,
        startOnTick: true,
        minPadding: 0
      },
      plotOptions: {
        series: {
          cursor: "pointer",
          allowPointSelect: true,
          point: {
            events: {
              select: this.ewsSelect.bind(this)
            }
          }
        }
      },
      series: this.EWSData,
      exporting: {
        enabled: false
      }
    });

    this.ewsHidden = new Chart({
      chart: {
        type: "line",
        height: 600,
        width: 1000
      },
      legend: {
        enabled: true,
        maxHeight: 60
      },
      credits: {
        enabled: false
      },
      title: {
        text: "Early Warning System"
      },
      yAxis: {
        max: 15,
        min: 0,
        title: { text: null },
        labels: {
          formatter: function() {
            var value = yAxisLabels[this.value];
            return value !== "undefined" ? value : this.value;
          }
        }
      },
      xAxis: {
        tickInterval: 1,
        categories: this.EWSXAxis,
        startOnTick: true,
        minPadding: 0
      },
      plotOptions: {
        series: {
          cursor: "pointer"
        }
      },
      series: this.EWSData,
      exporting: {
        enabled: false
      }
    });
    this.parent.progressRef.complete();
  }

  edited = false;
  ewsSelect(res) {
    this.subCategory = res.target.series.name;
    this.dateAxis = res.target.category;
    this.currentPage = 1;
    this.ToneChart = false;
    this.loadArticlesList(1, this.dateAxis, this.subCategory);
    this.loadArticleToneList(1, this.dateAxis, "parent", -1, this.subCategory);
  }

  onCategorySelected(res) {
    this.toneclick = false
    this.tone = res.target.series.name;
    this.media_id = "parent";
    this.dateAxis = "parent";
    this.subCategory = res.target.category;
    var toneStr =
      res.target.series.name == "Positive"
        ? 1
        : res.target.series.name == "Neutral"
        ? 0
        : -1;
    this.loadArticleToneList(
      1,
      this.dateAxis,
      this.media_id,
      toneStr,
      res.target.category
    );
  }

  onCoverageSelected(res) {
    this.toneclick = false
    this.ToneChart = true;
    this.tone = res.target.series.name;
    var toneStr =
      res.target.series.name == "Positive"
        ? 1
        : res.target.series.name == "Neutral"
        ? 0
        : -1;
    this.media_id = "parent";
    this.dateAxis = res.target.category;
    this.loadArticleToneList(
      1,
      this.dateAxis,
      this.media_id,
      toneStr,
      "parent"
    );
  }

  onMediaSelection(res) {
    this.toneclick = false
    for (let i = 0; i < this.mediaRawData.data.length; i++) {
      if (this.mediaRawData.data[i].media_name == res.target.category) {
        this.media_id = this.mediaRawData.data[i].media_id;
      }
    }
    this.media_name = res.target.category;
    this.ToneChart = true;
    this.tone = res.target.series.name;
    var toneStr =
      res.target.series.name == "Positive"
        ? 1
        : res.target.series.name == "Neutral"
        ? 0
        : -1;
    this.dateAxis = "parent";
    this.loadArticleToneList(
      1,
      this.dateAxis,
      this.media_id,
      toneStr,
      "parent"
    );
  }

  onPieSelect(res) {
    this.currentPage = 1;
    this.subCategory = res.target.options.name;
    this.dateAxis = "parent";
    this.ToneChart = false;
    this.toneclick = false;
    this.loadArticlesList(1, this.dateAxis, this.subCategory);
  }
  onPieSelectpercentage(res){
    this.toneclick = true
    this.media_id = "parent";
    this.dateAxis = "parent";
    var toneStr = res.target.name == "Positive"
        ? 1
        : res.target.name == "Neutral"
        ? 0
        : -1;
    this.loadArticleToneList(
      1,
      this.dateAxis,
      this.media_id,
      toneStr,
      res.target.category
    );
  }

  onPointSelect(res) {
    this.subCategory = res.target.series.name;
    this.dateAxis = res.target.category;
    this.currentPage = 1;
    this.ToneChart = false;
    this.loadArticlesList(1, this.dateAxis, this.subCategory);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.loadArticlesList(this.currentPage, this.dateAxis, this.subCategory);
  }
  showSearchModalmaps(article){
      interface UserResponse {
        data: Object;
      }
      let params = {
        article_id:article.article_id
      };
      this.http
        .post<UserResponse>(
          'https://kemlu.digivla.id/apidigivla/api/v1/article-by-id/',
          params,
          this.options
        )
        .subscribe(
          (result: any) => {
            this.parent.progressRef.complete();
            var res = [];
            res = result.data;
            var argumenta = [];
            var args = res.join().split(",");
            for (let i = 0; i < args.length; i++) {
              var argsExp = args[i].split('" "');
              if (argsExp.length > 1) {
                for (let j = 0; j < argsExp.length; j++) {
                  var re = new RegExp('"', "gi");
                  var rplc = argsExp[j].replace(re, "");
                  argumenta.push(rplc);
                }
              } else {
                var re = new RegExp('"', "gi");
                var rplc = args[i].replace(re, "");
                argumenta.push(rplc);
              }
            }
            highlightedWord = argumenta.join();
            var re = new RegExp(",", "gi");
            var highlightedWord = highlightedWord.replace(re, "||");
  
            this.searchModalContainer.contentModel =
            highlightedWord == ""
            ? this.highlightedsummary(article.content, article.category_id)
            : this.highlightedsummary(article.content, highlightedWord);
        this.searchModalContainer.filePdfModel = article.file_pdf;
        this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
          "."
        )[1];
            this.searchModalContainer.summaryModel =
              highlightedWord == ""
                ? this.highlightedsummary(article.content, article.category_id)
                : this.highlightedsummary(article.content, highlightedWord);
            this.searchModalContainer.filePdfModel = article.file_pdf;
            this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
              "."
            )[1];
            this.searchModalContainer.categoriesList = [];
            this.searchModalContainer.articleArr = article;
            if (article.title_edit === "" || article.title_edit === undefined) {
              this.searchModalContainer.titleModel = article.title;
            } else {
              this.searchModalContainer.titleModel = article.title_edit;
            }
            this.searchModalContainer.dateModel = article.datee;
            this.searchModalContainer.mediaModel = article.media_name;
            this.searchModalContainer.pageDesc = {
              print: true,
              pdf_text: false,
              doc: true,
              save: false,
              close: true,
              scan_media: true,
              text_print: true,
              link: true
            };
            this.searchModalContainer.urlFileModel =
              "https://input.digivla.id/media_tv/" +
              article.file_pdf.split("-")[0] +
              "/" +
              article.file_pdf.split("-")[1] +
              "/" +
              article.file_pdf.split("-")[2] +
              "/" +
              article.file_pdf;
            this.searchModalContainer.searchModal.show();
          },
          (err: any) => {
          }
        );
  }
  showSearchModal(article: any) {
    var highlightedWord;
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = { category_id: article.categories.join() };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/keywords/",
        param,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          var res = [];
          res = result.data;
          var argumenta = [];
          var args = res.join().split(",");
          for (let i = 0; i < args.length; i++) {
            var argsExp = args[i].split('" "');
            if (argsExp.length > 1) {
              for (let j = 0; j < argsExp.length; j++) {
                var re = new RegExp('"', "gi");
                var rplc = argsExp[j].replace(re, "");
                argumenta.push(rplc);
              }
            } else {
              var re = new RegExp('"', "gi");
              var rplc = args[i].replace(re, "");
              argumenta.push(rplc);
            }
          }
          highlightedWord = argumenta.join();
          var re = new RegExp(",", "gi");
          var highlightedWord = highlightedWord.replace(re, "||");

          this.searchModalContainer.contentModel =
            highlightedWord == ""
              ? this.highlighted(article.content, article.category_id)
              : this.highlighted(article.content, highlightedWord);
          this.searchModalContainer.summaryModel =
            highlightedWord == ""
              ? this.highlightedsummary(article.content, article.category_id)
              : this.highlightedsummary(article.content, highlightedWord);
          this.searchModalContainer.filePdfModel = article.file_pdf;
          this.searchModalContainer.mediaExtensionModel = article.file_pdf.split(
            "."
          )[1];
          this.searchModalContainer.categoriesList = [];
          this.searchModalContainer.articleArr = article;
          if (this.toneclick === true){
            this.searchModalContainer.titleTone = article.tone;
          }else{
            this.searchModalContainer.titleTone = '';
          }
          if (article.title_edit === "" || article.title_edit === undefined) {
            this.searchModalContainer.titleModel = article.title;
          } else {
            this.searchModalContainer.titleModel = article.title_edit;
          }
          this.searchModalContainer.dateModel = article.datee;
          this.searchModalContainer.mediaModel = article.media_name;
          this.searchModalContainer.pageDesc = {
            print: true,
            pdf_text: false,
            doc: true,
            save: false,
            close: true,
            scan_media: true,
            text_print: true,
            link: true
          };
          this.searchModalContainer.urlFileModel =
            "https://input.digivla.id/media_tv/" +
            article.file_pdf.split("-")[0] +
            "/" +
            article.file_pdf.split("-")[1] +
            "/" +
            article.file_pdf.split("-")[2] +
            "/" +
            article.file_pdf;
          this.searchModalContainer.searchModal.show();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, failed while loading data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }

  highlighted(value: any, args: any): any {
    let argsArray = [];
    let keyAray = [];

    if (args.includes("&&")) {
      argsArray = args.split("&&");
    } else if (args.includes("||")) {
      argsArray = args.split("||");
    } else {
      argsArray.push(args);
    }

    for (let i = 0; i < argsArray.length; i++) {
      var re = new RegExp(argsArray[i].trimEnd().trimStart(), "gi");
      value = value.replace(
        re,
        "<mark>" + argsArray[i].trimEnd().trimStart() + "</mark>"
      );
    }
    return value;
  }
  highlightedsummary(value: any, args: any): any {
    // let argsArray = args.split('&&')
    let argsArray = [];
    let keyAray = [];

    if (args.includes("&&")) {
      argsArray = args.split("&&");
    } else if (args.includes("||")) {
      argsArray = args.split("||");
    } else {
      argsArray.push(args);
    }
    for (let i = 0; i < argsArray.length; i++) {
      var re = new RegExp(argsArray[i].trimEnd().trimStart(), "gi");
    }
    return value;
  }
  loadArticleToneList(page, xAxis, media_id, tone, category) {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    var end;
    if (this.parent.periodModel == "4") {
      if (xAxis == "parent") {
        end = moment(this.parent.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();
      } else {
        var dateConverted = moment(xAxis).toDate();
        end = this.parent.addDays(dateConverted, 30);
        end = moment(end)
          .format("YYYY-MM-DD")
          .toString();
      }
    } else {
      if (xAxis == "parent") {
        end = moment(this.parent.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();
      } else {
        end = xAxis;
      }
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        category == "parent"
          ? this.parent.subCategoryModel == "All Sub Categroy"
            ? "all"
            : this.parent.subCategoryModel
          : category,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: media_id == "parent" ? this.parent.subMediaModel : media_id,
      tone: tone,
      start_date:
        xAxis == "parent"
          ? moment(this.parent.periodFromModel)
              .format("YYYY-MM-DD")
              .toString()
          : xAxis,
      end_date: end,
      maxSize: 10,
      page: page - 1
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/article-by-tone",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModal.show();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }

  loadArticlesList(page, xAxis, subCategory) {
    this.toneclick = false
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    var end;
    if (this.parent.periodModel == "4") {
      if (xAxis == "parent") {
        end = moment(this.parent.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();
      } else {
        var dateConverted = moment(xAxis).toDate();
        end = this.parent.addDays(dateConverted, 30);
        end = moment(end)
          .format("YYYY-MM-DD")
          .toString();
      }
    } else {
      if (xAxis == "parent") {
        end = moment(this.parent.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();
      } else {
        end = xAxis;
      }
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id: subCategory,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      term: null,
      start_date:
        xAxis == "parent"
          ? moment(this.parent.periodFromModel)
              .format("YYYY-MM-DD")
              .toString()
          : xAxis,
      end_date: end,
      maxSize: 10,
      page: page - 1
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "user/editing/",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModal.show();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
      this.toneclick = false
  }
  pageChangedmaps(event){
    this.currentPagemaps = event.page;
    this.loaddatalistmaps();
  }
  pageChangedtopic(event){
    this.currentPagetopic = event.page;
    this.loadclicktopic();
  }
  showSearchModaltopic(article){
    interface UserResponse {
      data: Object;
    }
    let params = {
      article_id:article.article_id
    };
    this.http
      .post<UserResponse>(
        this.parent.url+'topic/get-topic-article-by-id/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.searchModalContainer.contentModel = result.content
          this.searchModalContainer.summaryModel = result.content
          this.searchModalContainer.categoriesList = [];
          this.searchModalContainer.articleArr = article;
          if (article.title_edit === "" || article.title_edit === undefined) {
            this.searchModalContainer.titleModel = article.title;
          } else {
            this.searchModalContainer.titleModel = article.title_edit;
          }
          this.searchModalContainer.dateModel = result.datee;
          this.searchModalContainer.mediaModel = result.media_name;
          this.searchModalContainer.pageDesc = {
            print: true,
            pdf_text: false,
            doc: true,
            save: false,
            close: true,
            scan_media: true,
            text_print: true,
            link: true
          };
          this.searchModalContainer.urlFileModel =
            "https://input.digivla.id/media_tv/" +
            result.file_pdf.split("-")[0] +
            "/" +
            result.file_pdf.split("-")[1] +
            "/" +
            result.file_pdf.split("-")[2] +
            "/" +
            result.file_pdf;
          this.searchModalContainer.searchModal.show();
        },
        (err: any) => {
        }
      );
  }
  pptHandler() {
    this.parent.progressRef.start();
    this.chartArray = [];
    var className = document.getElementById("hidenDiv").querySelectorAll("svg");
    for (let i = 0; i < className.length; i++) {
      var result = new XMLSerializer().serializeToString(className[i]);
      var base64str =
        "data:image/svg+xml;base64," +
        btoa(unescape(encodeURIComponent(result)));

      let im = new Image();
      im.src = base64str;

      let can = document.createElement("canvas");
      let ctx = can.getContext("2d");

      let self = this;
      im.onload = function() {
        can.width = im.width;
        can.height = im.height;
        ctx.drawImage(im, 0, 0, im.width, im.height);
        self.chartArray.push(can.toDataURL("image/png"));
        if (i == className.length - 1) {
          interface UserResponse {
            data: Object;
          }
          self.http
            .post<UserResponse>(
              self.parent.url + "user/downloads/pptx",
              self.chartArray,
              self.options
            )
            .subscribe(
              (result: any) => {
                self.parent.progressRef.complete();
                window.open(result.data);
              },
              (err: any) => {
                self.alerts = [];
                self.parent.progressRef.complete();
                self.alerts.push({
                  type: "danger",
                  msg: "snap, error while loading data.",
                  timeout: 3000
                });
              }
            );
        }
      };
    }
  }

  xlsHandler() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.parent.groupCategoryModel,
      category_id:
        this.parent.subCategoryModel == "All Sub Categroy"
          ? "all"
          : this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/download-excel",
        editingParam,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          window.open(result.data);
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  getMediaGroup() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }
      this.http
        .get<UserResponse>(this.url + "user/medias/", this.options)
        .subscribe((result: any) => {
          this.groupMediaOption = result.results;
          this.parent.groupMediaModel = this.groupMediaOption[0].user_media_type_id;
          resolve(this.parent.groupMediaModel);
        });
    });
  }

  getSubMedias() {
    // add for EWS Ahmad
    interface UserResponse {
      data: Object;
    }
    this.http
      .get<UserResponse>(
        this.url + "user/submedias/" + this.parent.groupMediaModel,
        this.options
      )
      .subscribe((result: any) => {
        this.subMediaOption = result.results;
        if (this.subMediaOption.length < 1) {
          if (this.parent.groupMediaModel == "0") {
            this.subMediaOption.unshift({
              media_id: 0,
              media_name: "All Sub Media",
              media_type_id: 1,
              circulation: 35700,
              rate_bw: 117000,
              rate_fc: 195000,
              language: "IND",
              statuse: "A",
              usere: "1",
              pc_name: "",
              input_date: "2012-11-22T11:05:42Z",
              tier: 2
            });
          } else {
            this.subMediaOption.unshift({
              media_id: 0,
              media_name: "No Sub Media",
              media_type_id: 1,
              circulation: 35700,
              rate_bw: 117000,
              rate_fc: 195000,
              language: "IND",
              statuse: "A",
              usere: "1",
              pc_name: "",
              input_date: "2012-11-22T11:05:42Z",
              tier: 2
            });
          }
        } else {
          this.subMediaOption.unshift({
            media_id: 0,
            media_name: "All Sub Media",
            media_type_id: 1,
            circulation: 35700,
            rate_bw: 117000,
            rate_fc: 195000,
            language: "IND",
            statuse: "A",
            usere: "1",
            pc_name: "",
            input_date: "2012-11-22T11:05:42Z",
            tier: 2
          });
        }
        this.parent.subMediaModel = this.subMediaOption[0].media_id;
      });
  }

  ngOnInit(): void {
    this.loadMediaVisibility();
    this.loadTonality();
    this.loadTrendingHighlights();
    this.loadToneByCategory();
    this.loadToneByMedia();

    this.getMediaGroup().then(res => {
      this.getSubMedias();
      this.loadEWS();
    });

    this.subscription = this.childListener.notifyObservable$.subscribe(res => {
      if (res.hasOwnProperty("option") && res.option === "call_child") {
        this.loadEWS();
        this.loadMediaVisibility();
        this.loadTonality();
        this.loadTrendingHighlights();
        this.loadToneByCategory();
        this.loadToneByMedia();
      }
    });
  }
  renderwordcloud(){
    let datawordcloud = [];
    interface UserResponse {
      data: Object;
    }
    let params = {
      category_set: this.parent.groupCategoryModel,
      category_id: this.parent.subCategoryModel == "All Sub Categroy"? "all": this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };
    this.http
      .post<UserResponse>(
        this.parent.url+'dashboard/wordcloud',
        // this.parent.url + "dashboard/wordcloud",
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          result.data.data_wcloid.forEach(element => {
              let dataobj = {
                name:element.name,
                weight:element.value
              }
              datawordcloud.push(dataobj)
          });
          this.datanewstrend = [];
          result.data.data_topic.forEach(element => {
            let dataobj = {
              name:element.name,
              weight:element.value
            }
            this.datanewstrend.push(dataobj)
        });
          this.wordcloud = new Chart({
            title: {
              text: null
            },
            series: [{
              type: 'wordcloud',
              data: datawordcloud,
              name: 'Occurrences'
          }],
          });
          this.wordcloudhidden = new Chart({
            title: {
              text: null
            },
            series: [{
              type: 'wordcloud',
              data: datawordcloud,
              name: 'Occurrences'
          }],
          });
        },
        (err: any) => {
          // this.alerts = [];
          // this.parent.progressRef.complete();
          // this.alerts.push({
          //   type: "danger",
          //   msg: "snap, error while loading data.",
          //   timeout: 3000
          // });
        }
      );
  }
  clicktopic(data){
    this.topicgetclick = data.name;
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      topic : this.topicgetclick,
      page: this.currentPagetopic - 1
  }
    this.http
      .post<UserResponse>(
        this.parent.url+"topic/get-topic-article/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {    
          this.articlesList = [];
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModaltopic.show()
        },
        (err: any) => {
        }
      );
  }
  loadclicktopic(){
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      topic : this.topicgetclick,
      page: this.currentPagetopic - 1
  }
    this.http
      .post<UserResponse>(
        this.parent.url+"topic/get-topic-article/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {    
          this.articlesList = [];
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModaltopic.show()
        },
        (err: any) => {
        }
      );
  }
  renderdatachart() {
    this.statelocal.getlocalgeojson().subscribe(data => {
      interface UserResponse {
        data: Object;
      }
      let dataallloc = [];
      let body = {
        start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
        end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString()
      }
      this.http.post<UserResponse>("https://kemlu.digivla.id/apidigivla/api/v1/all-count/", body , this.options)
        .subscribe(
          (result: any) => {
            result.data.forEach(element => {
              dataallloc.push([element.value,element.key.split('KABUPATEN ')[1]])
            });
            let interval = 0;
            this.customSeries = []
            dataallloc.forEach((item) => {
              if (item[0] > 0) {
                this.seriescolor = this.calculateDark(this.maxcolor, interval);
              } else {
                this.seriescolor = this.mincolor;
              }
              this.customSeries.push({
                name: item[1],
                percent: item[0],
                color: this.seriescolor,
                data: [{ 'value': item[0], 'name': item[1] }],
                borderColor: '#212121',
                states: {
                  hover: {
                    color: '#52DE97',
                  },
                },
              })
              interval += 0.2;
            })

            this.optionsmap = {
              chart: {
                backgroundColor: 'rgba(255, 255, 255, 0.0)',
                height: 300
              },
              title: {
                text: ''
              },
              mapNavigation: {
                enabled: true
            },
              legend: {
                enabled: false,
                symbolRadius: 0,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 0,
                itemStyle: {
                  color: '#c9c9ca',
                },
                itemHoverStyle: {
                  color: '#FFFFFF',
                },
                itemHiddenStyle: {
                  color: '#414144',
                },
                useHTML: true,
                navigation: {
                  activeColor: '#cccccc',
                  inactiveColor: '#737373',
                  style: {
                    color: '#4a4a4a',
                    fontSize: '15px',
                  },
                  buttonOptions: {
                    enabled: false
                    }
                },
              },
              plotOptions: {
                map: {
                  allAreas: false,
                  joinBy: ['name'],
                  mapData: data,
                  tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.value}</b>'
                  }

                },
                series:{
                  point:{
                      events:{
                        click: (event)=>{
                        	this.onclickmaps(event);
                        }
                        }
                    }
                }
              },
              credits: {
                enabled: false
              },
              series: this.customSeries
            }
            this.mapChart = new MapChart(this.optionsmap); 
            this.mapCharthidden = new MapChart(this.optionsmap); 
          },
          err => { 
          }
        );
    })
  }
  LightenDarkenColor(col, amt) {
    this.calculateDark(col, amt)
  }
  calculateDark(col, amt) {
    var usePound = false;
    if (col[0] == '#') {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) - amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 0;
    var b = (num >> 8) & 0x00ff;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;
    var g = num & 0x0000ff;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  }
  onclickmaps(event){
    this.geoname = event.point.name;
    interface UserResponse {
      data: Object;
    }
    let params = {
      start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      page: 0,
      max_size: 10,
      geo_loc: this.geoname
    };
    this.http
      .post<UserResponse>(
        'https://kemlu.digivla.id/apidigivla/api/v1/article-by-geo/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModalmaps.show();
        },
        (err: any) => {
        }
      );
  }
  loaddatalistmaps(){
    interface UserResponse {
      data: Object;
    }
    let params = {
      start_date: (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      end_date: (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      page: this.currentPagemaps - 1,
      max_size: 10,
      geo_loc: this.geoname
    };
    this.http
      .post<UserResponse>(
        'https://kemlu.digivla.id/apidigivla/api/v1/article-by-geo/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.articlesList = [];
          this.articlesList = result;
        },
        (err: any) => {
        }
      );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
