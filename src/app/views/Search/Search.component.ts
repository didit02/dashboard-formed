import { Component, OnInit,ViewChild,Inject,OnDestroy } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {DefaultLayoutComponent} from '../../containers/default-layout/default-layout.component'
import { moment } from 'ngx-bootstrap/chronos/test/chain';

import { Subscription } from 'rxjs/Subscription';
import { ChildListenerService } from '../../views/Services/ChildListener.service';
@Component({
  selector: 'app-Search',
  templateUrl: './Search.component.html',
  styleUrls: ['./Search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  searchModel;  
  alerts;
  searchFieldModel='title';
  mediaCategoryModel='0';
  mediaCategoryOption:any[]=[];


  articlesFound:any=[];
  totalRow:number;
  pageSize:number=10;
  currentPage:number =1;

  isVisible:boolean=false;
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };

  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent, private http: HttpClient,private childListener: ChildListenerService) { }

  @ViewChild('searchModalContainer') public searchModalContainer;  

  showSearchModal(article:any){    
    this.searchModalContainer.articleArr = article;
    if (article.title_edit === '' || article.title_edit === undefined){
      this.searchModalContainer.titleModel = article.title;
    }else{
      this.searchModalContainer.titleModel = article.title_edit;
    }    this.searchModalContainer.dateModel = article.datee;
    this.searchModalContainer.mediaModel = article.media_name;
    this.searchModalContainer.contentModel = this.highlighted(article.content,this.searchModel);
    this.searchModalContainer.summaryModel = this.highlightedsummary(article.content,this.searchModel);
    this.searchModalContainer.filePdfModel = article.file_pdf;
    this.searchModalContainer.mediaExtensionModel = article.file_pdf.split('.')[1];
    this.searchModalContainer.pageDesc = {
      'print':true,
      'pdf_text':true,
      'doc':true,
      'save':true,
      'close':true
    };
    this.searchModalContainer.urlFileModel = "http://input.digivla.id/"+ article.media_type +"/"+ article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;
    this.searchModalContainer.searchModal.show();
  }

  highlighted(value: any, args: any): any {
    let argsArray=[];
    let keyAray=[];
    if (args.includes('&&')) { 
      argsArray = args.split('&&');         
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    for (let i=0; i < argsArray.length;i++){
      var re = new RegExp((argsArray[i].trimEnd()).trimStart(), 'gi');
      value = value.replace(re, "<mark>" + (argsArray[i].trimEnd()).trimStart() + "</mark>");  
    }
    return value;
  }
  highlightedsummary(value: any, args: any): any {
    let argsArray=[];
    let keyAray=[];

    if (args.includes('&&')) {
      argsArray = args.split('&&');
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    for (let i=0; i < argsArray.length;i++){
      var re = new RegExp((argsArray[i].trimEnd()).trimStart(), 'gi');
    }
    return value;
  }

  searchFunction(page){
    this.parent.progressRef.start();
    this.isVisible=true;
    interface UserResponse {
      data: Object;
    }
    if (page==null){
      page=1;
    }
    let paramSearch={
      media_category:this.mediaCategoryModel,
      start_date:(moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      end_date:(moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      search_field:this.searchFieldModel,
      term:this.searchModel,
      page:page-1,
      maxSize:this.pageSize
    }
    
    this.http.post<UserResponse>(this.parent.url +'search/',paramSearch,this.options)
    .subscribe((result: any) => {
      this.articlesFound = result;
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }
  pageChanged(event: any): void {
    
    this.currentPage= event.page;
    this.searchFunction(event.page);    
  }
  clickSearch(){
    this.currentPage=1;
    this.searchFunction(1);    
  }
  ngOnInit() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.parent.url+'media-categories/',this.options)
    .subscribe((result: any) => {
      this.mediaCategoryOption = result.results;    
      this.mediaCategoryModel = this.mediaCategoryOption[0].key;
    });

    this.subscription = this.childListener.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'call_child') {
              
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
