import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JournalistConnectComponent } from './JournalistConnect.component';
import { JournalistConnectRoutingModule } from './JournalistConnect-routing.module';

import { NgxEditorModule } from 'ngx-editor';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
@NgModule({
  imports: [
    CommonModule,
    JournalistConnectRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    NgxEditorModule
  ],
  declarations: [JournalistConnectComponent]
})
export class JournalistConnectModule { }
