import { Component, OnInit,Inject } from '@angular/core';
import { DefaultLayoutComponent } from '../../containers/default-layout/default-layout.component'
import { HttpClient,HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-JournalistConnect',
  templateUrl: './JournalistConnect.component.html',
  styleUrls: ['./JournalistConnect.component.scss']
})
export class JournalistConnectComponent implements OnInit {
  
  alerts;
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };
  
  selectedFile: ImageSnippet;
  headlineModel;
  sublineModel;
  editorialModel;
  medianameArray=[]; 
  htmlContent;
  fileArray=[];
  
  jornalistList:any=[];
  editorConfig = {
      "editable": true,
      "spellcheck": true,
      "height": "auto",
      "minHeight": "350px",
      "width": "auto",
      "minWidth": "0",
      "translate": "yes",
      "enableToolbar": true,
      "showToolbar": true,
      "placeholder": "Enter text here...",
      "imageEndPoint": "",
      "toolbar": [
          ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
          ["fontName", "fontSize"],
          ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
          ["cut", "copy", "delete", "undo", "redo"],
          ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
          ["link", "unlink"]
      ]
  };
  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,private http: HttpClient) { }
  sendHandler(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let connectParam={
      "headline":this.headlineModel,
      "subline":this.sublineModel,
      "content":this.htmlContent,
      "editorial_desk": this.editorialModel,
      "media_names": this.medianameArray,
      "images": this.fileArray
    };
    
    this.http.post<UserResponse>(this.parent.url +'wartawan/upload',connectParam,this.options)
    .subscribe((result: any) => {         
      (result);
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  onCheckedChange(details,index){
    if (details.chosen == true){
      this.medianameArray.push(details.media_name);
    }else{
      this.medianameArray.splice(index,1);
    }
  }  
  
  processFile(imageInput: any) {
    const file: File = imageInput.files[0];    
    const reader = new FileReader();   
    
    this.getBase64(file).then(data => {
        this.fileArray.push({'filename': file.name, 'base64':data});
    });
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  loadJournalist(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    this.http.get<UserResponse>(this.parent.url +'wartawan/medias',this.options)
    .subscribe((result: any) => {
      result.results.forEach(function(element) { element.checked = false; });       
      this.jornalistList = result;
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    });   
  }
  ngOnInit() {
    this.loadJournalist();
  }

}

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}