import { Component, OnInit,Inject,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {environment} from '../../../environments/environment'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent { 
  ModelEmail:string ="";
  ModelPassword:string="";
  clients: any;
  showSelected=false;
  msgerr:any;
  url:any=environment.apiUrl;
  modalRef: BsModalRef;
  imagelogin:any;
  @ViewChild('popUpHandlerLogin') public popUpHandlerLogin;
  public loading = false;
  constructor(private http: HttpClient,private router: Router,private BsmodalService: BsModalService) { }  
  
  login(){
    this.loading = true;
    let fd = {
      username:this.ModelEmail,
      password:this.ModelPassword,
    }; 
    
    interface UserResponse {
      data: Object;
    }
    this.http.post<UserResponse>(this.url+ 'login/', fd)
    .subscribe(result => {
      this.clients = result;
      
      if (this.clients.code != '401'){
        localStorage.setItem('user', this.clients.usr_comp_login);
        localStorage.setItem('token', this.clients.token);
        localStorage.setItem('client', this.clients.client_id);
        localStorage.setItem('logo', this.clients.comp_icon);
        localStorage.setItem('levelmenu',this.clients.usr_comp_level)
        this.router.navigateByUrl('/dashboard');
      }else{
        this.loading = false;
        this.msgerr = this.clients.message;
        this.popUpHandlerLogin.show();
      }
      }, error => {
        this.loading = false;
        this.msgerr ="not connected...";
        this.popUpHandlerLogin.show();

      });  
}

  ngOnInit(): void {
    if (window.location.host === 'djsn-formed.onlinemonitoring.id'){
      this.imagelogin = '/assets/img/logo_client/kmm.png'
    }else{
      this.imagelogin = 'assets/img/brand/digivla-color.png'
    }
  }
}
