import { NewsClippingComponent } from "./NewsClipping.component";

import { NewsClippingRoutingModule } from "./NewsClipping-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
// import {AutocompleteLibModule} from 'angular-ng-autocomplete';

import { PaginationModule } from "ngx-bootstrap/pagination";
import { FormsModule } from "@angular/forms";
import { AlertModule } from "ngx-bootstrap/alert";
import { ReactiveFormsModule } from "@angular/forms";

import { ArticleDetailsModule } from "../ArticleDetails/ArticleDetails.module";
import { ModalModule } from "ngx-bootstrap/modal";

@NgModule({
  imports: [
    CommonModule,
    NewsClippingRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    ArticleDetailsModule,
    // AutocompleteLibModule
  ],
  declarations: [NewsClippingComponent],
})
export class NewsClippingModule {}
