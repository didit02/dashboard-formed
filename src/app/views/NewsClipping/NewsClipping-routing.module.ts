import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { NewsClippingComponent } from './NewsClipping.component';

const routes: Routes = [
  {
    path: '',
    component: NewsClippingComponent,
    data: {
      title: 'News Clipping'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsClippingRoutingModule {}
