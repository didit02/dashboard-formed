import { Component, OnInit, OnDestroy, ViewChild, Inject } from "@angular/core";
import {Chart } from "angular-highcharts";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { ListenerService } from "../../views/Services/listener.service";

@Component({
  selector: "app-Trends",
  templateUrl: "./Trends.component.html",
  styleUrls: ["./Trends.component.scss"],
})
export class TrendsComponent implements OnInit {
  alerts:any;
  wordcloud = {};
  datanewstrend = [];
  topicgetclick:any;
  currentPagetopic:number = 1;
  articlesList: any = [];
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token")
  });
  options = { headers: this.headers };
  @ViewChild("showArticlesModaltopic") public showArticlesModaltopic;
  @ViewChild("searchModalContainer") public searchModalContainer;
  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
  private http: HttpClient) {}

  ngOnInit() {
    this.renderwordcloud()
  }
  showSearchModaltopic(article){
    interface UserResponse {
      data: Object;
    }
    let params = {
      article_id:article.article_id
    };
    this.http
      .post<UserResponse>(
        this.parent.url+'topic/get-topic-article-by-id/',
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.searchModalContainer.contentModel = result.content
          this.searchModalContainer.summaryModel = result.content
          this.searchModalContainer.categoriesList = [];
          this.searchModalContainer.articleArr = article;
          if (article.title_edit === "" || article.title_edit === undefined) {
            this.searchModalContainer.titleModel = article.title;
          } else {
            this.searchModalContainer.titleModel = article.title_edit;
          }
          this.searchModalContainer.dateModel = result.datee;
          this.searchModalContainer.mediaModel = result.media_name;
          this.searchModalContainer.pageDesc = {
            print: true,
            pdf_text: false,
            doc: true,
            save: false,
            close: true,
            scan_media: true,
            text_print: true,
            link: true
          };
          this.searchModalContainer.urlFileModel =
            "https://input.digivla.id/media_tv/" +
            result.file_pdf.split("-")[0] +
            "/" +
            result.file_pdf.split("-")[1] +
            "/" +
            result.file_pdf.split("-")[2] +
            "/" +
            result.file_pdf;
          this.searchModalContainer.searchModal.show();
        },
        (err: any) => {
        }
      );
  }
  renderwordcloud(){
    let datawordcloud = [];
    interface UserResponse {
      data: Object;
    }
    let params = {
      category_set: this.parent.groupCategoryModel,
      category_id: this.parent.subCategoryModel == "All Sub Categroy"? "all": this.parent.subCategoryModel,
      user_media_type_id: this.parent.groupMediaModel,
      media_id: this.parent.subMediaModel,
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };
    this.http
      .post<UserResponse>(
        this.parent.url+'dashboard/wordcloud',
        // this.parent.url + "dashboard/wordcloud",
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          result.data.data_wcloid.forEach(element => {
              let dataobj = {
                name:element.name,
                weight:element.value
              }
              datawordcloud.push(dataobj)
          });
          this.datanewstrend = [];
          result.data.data_topic.forEach(element => {
            let dataobj = {
              name:element.name,
              weight:element.value
            }
            this.datanewstrend.push(dataobj)
        });
          this.wordcloud = new Chart({
            title: {
              text: null
            },
            series: [{
              type: 'wordcloud',
              data: datawordcloud,
              name: 'Occurrences'
          }],
          });
        },
        (err: any) => {
          this.alerts = [];
          this.parent.progressRef.complete();
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
        }
      );
  }

  clicktopic(data){
    this.topicgetclick = data.name;
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      topic : this.topicgetclick,
      page: this.currentPagetopic - 1
  }
    this.http
      .post<UserResponse>(
        this.parent.url+"topic/get-topic-article/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {    
          this.articlesList = [];
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModaltopic.show()
        },
        (err: any) => {
        }
      );
  }
  pageChangedtopic(event){
    this.currentPagetopic = event.page;
    this.loadclicktopic();
  }
  loadclicktopic(){
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      topic : this.topicgetclick,
      page: this.currentPagetopic - 1
  }
    this.http
      .post<UserResponse>(
        this.parent.url+"topic/get-topic-article/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {    
          this.articlesList = [];
          this.articlesList = result;
          this.parent.progressRef.complete();
          this.showArticlesModaltopic.show()
        },
        (err: any) => {
        }
      );
  }
}
