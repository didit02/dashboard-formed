import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { MapChart } from 'angular-highcharts';
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { Subscription } from "rxjs";
import { ListenerService } from "../Services/listener.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { DefaultLayoutComponent } from '../../containers/default-layout/default-layout.component'
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
  mapChart: any;
  options: any;
  customSeries: any;
  seriescolor: any;
  mincolor: string = '#D3D3D3';
  maxcolor: string = '#BFFFB2';
  subscription: Subscription;
  today = new Date();
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  loadermaps:boolean=false;
  showmaps:boolean=false;
  loading:boolean = false;
  contentmaps:any;
  paramsMap:any;
  @ViewChild("articlesContainer") public articlesContainer;
  constructor(
    private statelocal: ListenerService,
    private http: HttpClient,
    private childListener: ChildListenerService,
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent
  ) { }

  ngOnInit() {
    this.paramsMap={
      "start_date": (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),                
    };
    this.renderdata()
    this.subscription = this.childListener.notifyObservable$.subscribe((res) => {            
      if (res.hasOwnProperty('option') && res.option === 'call_child') {        
        this.paramsMap={
          "start_date": (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
          "end_date": (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),                
        };
        this.renderdata()        
      } else if (res.hasOwnProperty('option') && res.option === 'save_article') {
      }
    });
  }
  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }
  renderdata() {
    this.loading  = true          
    this.statelocal.getlocalgeojson().subscribe(data => {
      interface UserResponse {
        data: Object;
      }
      let dataallloc = [];
      const headers =  new HttpHeaders().set('Content-Type', 'application/json')
      const options = { headers: headers };
      let body = new URLSearchParams();
      body.set('start_date', (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString());
      body.set('end_date', (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString());
      let datagetname = [];
      let datacomparename  = [];
      let listdatakota = ["BANDA ACEH","SABANG"
      ,"LANGSA"
      ,"LHOKSEUMAWE"
      ,"SIBOLGA"
      ,"TANJUNG BALAI"
      ,"PEMATANG SIANTAR"
      ,"TEBING TINGGI"
      ,"MEDAN"
      ,"BINJAI"
      ,"PADANG SIDEMPUAN"
      ,"PADANG"
      ,"SOLOK"
      ,"SAWAH LUNTO"
      ,"PADANG PANJANG"
      ,"BUKITTINGGI"
      ,"PAYAKUMBUH"
      ,"PARIAMAN"
      ,"PEKANBARU"
      ,"DUMAI"
      ,"JAMBI"
      ,"PALEMBANG"
      ,"PRABUMULIH"
      ,"PAGAR ALAM"
      ,"LUBUK LINGGAU"
      ,"BANDAR LAMPUNG"
      ,"METRO"
      ,"PANGKAL PINANG"
      ,"BATAM"
      ,"TANJUNG PINANG"
      ,"JAKARTA SELATAN"
      ,"JAKARTA TIMUR"
      ,"JAKARTA PUSAT"
      ,"JAKARTA BARAT"
      ,"JAKARTA UTARA"
      ,"BOGOR"
      ,"SUKABUMI"
      ,"BANDUNG"
      ,"CIREBON"
      ,"BEKASI"
      ,"DEPOK"
      ,"CIMAHI"
      ,"TASIKMALAYA"
      ,"BANJAR"
      ,"MAGELANG"
      ,"SURAKARTA"
      ,"SALATIGA"
      ,"SEMARANG"
      ,"PEKALONGAN"
      ,"TEGAL"
      ,"YOGYAKARTA"
      ,"KEDIRI"
      ,"BLITAR"
      ,"MALANG"
      ,"PROBOLINGGO"
      ,"PASURUAN"
      ,"MOJOKERTO"
      ,"MADIUN"
      ,"SURABAYA"
      ,"BATU"
      ,"TANGERANG"
      ,"CILEGON"
      ,"DENPASAR"
      ,"MATARAM"
      ,"BIMA"
      ,"KUPANG"
      ,"PONTIANAK"
      ,"SINGKAWANG"
      ,"PALANGKA RAYA"
      ,"BANJARMASIN"
      ,"BANJAR BARU"
      ,"BALIKPAPAN"
      ,"SAMARINDA"
      ,"BONTANG"
      ,"TARAKAN"
      ,"MANADO"
      ,"BITUNG"
      ,"TOMOHON"
      ,"PALU"
      ,"MAKASSAR"
      ,"PARE-PARE"
      ,"PALOPO"
      ,"KENDARI"
      ,"BAUBAU"
      ,"GORONTALO"
      ,"AMBON"
      ,"TERNATE"
      ,"TIDORE KEPULAUAN"
      ,"SORONG"
      ,"JAYAPURA"
      ]
      let datause;
      let datasplitkab;
      this.http.get<UserResponse>("./apidigivla/all-count/?" + body , options)
        .subscribe(
          (result: any) => {
            for (let i = 0; i < result['data'].length; i++) {
              if (listdatakota.includes(result['data'][i].key.split('KABUPATEN ')[1]) === true){
                datause = [result['data'][i].value, result['data'][i].key.replace('KABUPATEN' , 'KOTA')]
              }else{
                datause = [result['data'][i].value, result['data'][i].key]
              }          
              datacomparename.push(result['data'][i].key.split('KABUPATEN ')[1])    
              dataallloc.push(datause)
            }
            for (let i = 0; i < data['features'].length; i++) {
              if (data['features'][i]['properties']['name'].includes('KABUPATEN') === true){
                datasplitkab = data['features'][i]['properties']['name'].split('KABUPATEN ')[1]
              }else{
                datasplitkab = data['features'][i]['properties']['name'].split('KOTA ')[1]
              }
              datagetname.push(datasplitkab)
            }              
            const arr1 = datagetname;
            const arr2 = datacomparename;

            let unique1 = arr1.filter((o) => arr2.indexOf(o) === -1);
            let unique2 = arr2.filter((o) => arr1.indexOf(o) === -1);

            const unique = unique1.concat(unique2);
            let nameuse;
            for (let i = 0; i < unique.length; i++) {
              if (listdatakota.includes(unique[i]) === true){
                nameuse =[0, 'KOTA ' + unique[i]]
              }else{
                nameuse =[0, 'KABUPATEN ' + unique[i]]
              }    
              dataallloc.push(nameuse)
            }            
            let interval = 0;
            this.customSeries = []
            dataallloc.forEach((item) => {
              if (item[0] > 0) {
                this.seriescolor = this.calculateDark(this.maxcolor, interval);
              } else {
                this.seriescolor = this.mincolor;
              }
              this.customSeries.push({
                name: item[1],
                percent: item[0],
                color: this.seriescolor,
                data: [{ 'value': item[0], 'name': item[1] }],
                borderColor: '#212121',
                states: {
                  hover: {
                    color: '#52DE97',
                  },
                },
              })
              interval += 0.2;
            })

            this.options = {
              chart: {
                backgroundColor: 'rgba(255, 255, 255, 0.0)',
                height: 300
              },

              title: {
                text: ''
              },
              mapNavigation: {
                enabled: true,
                buttonOptions: {
                  verticalAlign: 'bottom'
                },
                buttons: {
                  zoomIn: {
                    style: {
                      opacity: 0.2,
                    },
                  },
                  zoomOut: {
                    style: {
                      opacity: 0.2,
                    },
                  }
                },

              },
              legend: {
                enabled: true,
                symbolRadius: 0,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 0,
                itemStyle: {
                  color: '#c9c9ca',
                },
                itemHoverStyle: {
                  color: '#FFFFFF',
                },
                itemHiddenStyle: {
                  color: '#414144',
                },
                useHTML: true,
                navigation: {
                  activeColor: '#cccccc',
                  inactiveColor: '#737373',
                  style: {
                    color: '#4a4a4a',
                    fontSize: '15px',
                  },
                  buttonOptions: {
                    enabled: false
                    }
                },
              },
              plotOptions: {
                map: {
                  allAreas: false,
                  joinBy: ['name'],
                  mapData: data,
                  tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.value}</b>'
                  }

                },
              },
              credits: {
                enabled: false
              },
              series: this.customSeries
            }
            this.mapChart = new MapChart(this.options); 
            this.loading  = false;
            this.showmaps = true;
          },
          err => { 
            this.contentmaps = err.statusText;
            this.loadermaps = true;
            this.loading  = false;
          }
        );
    })
  }
  LightenDarkenColor(col, amt) {
    this.calculateDark(col, amt)
  }
  calculateDark(col, amt) {
    var usePound = false;
    if (col[0] == '#') {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) - amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 0;
    var b = (num >> 8) & 0x00ff;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;
    var g = num & 0x0000ff;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  }
  onclickmaps(event){
    let namesplitkab;
    const start = moment(this.periodFromModel)
          .format("YYYY-MM-DD")
          .toString();
        const end = moment(this.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();     
          if (event.point.name.includes('KOTA') === true){
              namesplitkab = event.point.name.replace('KOTA' , 'KABUPATEN')
          } else{ 
            namesplitkab = event.point.name
          } 
    this.articlesContainer.loadArticlesmaps(namesplitkab , this.paramsMap.start_date , this.paramsMap.end_date , 1);
  }

}
