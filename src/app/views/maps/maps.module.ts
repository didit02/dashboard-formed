import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsRoutingModule } from './maps-routing.module';
import { MapsComponent } from "./maps.component";
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import { ArticlesModuleModule } from "../ArticlesModule/ArticlesModule.module";
import * as highmaps from 'highcharts/modules/map.src';
import { NgxLoadingModule } from 'ngx-loading';
@NgModule({
  imports: [
    CommonModule,
    MapsRoutingModule,
    ChartModule,
    ArticlesModuleModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [
    MapsComponent
  ],
  providers : [{ provide: HIGHCHARTS_MODULES, useFactory: () => [highmaps] }],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class MapsModule { }
