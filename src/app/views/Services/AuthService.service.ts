import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { HttpClient} from '@angular/common/http';
@Injectable()
export class AuthServiceService implements CanActivate{
  constructor(private router: Router,private http: HttpClient) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {  
    if (localStorage.getItem('token') == undefined || localStorage.getItem('token') == '') {
      this.router.navigateByUrl('/login');  
      return false;
    }else{
      return true;
    }   
  }
}
