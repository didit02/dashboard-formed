import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from "rxjs/operators"; 
@Injectable({
  providedIn: 'root'
})
export class ListenerService {
  private _datamaps: any;
  private _datinfluencer: any;
  constructor(
    private http: HttpClient
  ) { }

  getlocalgeojson(): Observable<any> {
    return this.http.get('assets/maps/allplacecompress.json').pipe(map(res => res));
  }

  set datamaps(val: any) {
    this._datamaps = val;
  }
  get datamaps(): any {
    return this._datamaps;
  }
  set eventinfluencer(val: any) {
    this._datinfluencer = val;
  }
  get eventinfluencer(): any {
    return this._datinfluencer;
  }
}
