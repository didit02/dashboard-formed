/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ChildListenerService } from './ChildListener.service';

describe('Service: ChildListener', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChildListenerService]
    });
  });

  it('should ...', inject([ChildListenerService], (service: ChildListenerService) => {
    expect(service).toBeTruthy();
  }));
});
