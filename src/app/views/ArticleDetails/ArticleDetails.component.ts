import { Component, OnInit,ViewChild,TemplateRef,ViewEncapsulation ,ChangeDetectorRef} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgProgress,NgProgressRef } from '@ngx-progressbar/core'
import { environment } from '../../../environments/environment'
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { _localeFactory } from '@angular/core/src/application_module';


@Component({
  selector: 'app-ArticleDetails',
  templateUrl: './ArticleDetails.component.html',
  styleUrls: ['./ArticleDetails.component.scss'],
  encapsulation: ViewEncapsulation.None  
})
export class ArticleDetailsComponent implements OnInit {
  url:any=environment.apiUrl;
  articleArr:any=[];
  titleModel:string;
  titleTone:string;
  contentModel:string;
  summaryModel:string;
  dateModel:string;
  mediaModel:string;
  filePdfModel:string;
  mediaExtensionModel:string;
  urlFileModel:string;
  category_id:any;
  categoriesList:any=[];
  progressRef: NgProgressRef;
  category_idMentioned:any;
  toneshow:boolean = false;
  pageDesc={
    'print':true,
    'pdf_text':true,
    'doc':true,
    'save':true,
    'close':true,
    'link':true,
    'scan_media':true,
    'text_print':true
  }
  
  @ViewChild('searchModal') public searchModal: ModalDirective;
  @ViewChild('videoPlayer') public videoPlayer;

  @ViewChild('saveArticlesContainer') public saveArticleContainer;
  
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };
  
  alerts;
  modalRef: BsModalRef;

  constructor(private http: HttpClient,public progress: NgProgress,private modalService: BsModalService,private childListener: ChildListenerService,private ref: ChangeDetectorRef) { }

  searchModalClose(){
    if (this.mediaExtensionModel=='mp4'){
      this.videoPlayer.nativeElement.pause();
    }
    this.searchModal.hide();
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height='+screen.availWidth+',width='+screen.availWidth);
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Digivla Dashboard</title>
          <style>
            .modal-body {
              position: relative;
              -ms-flex: 1 1 auto;
              flex: 1 1 auto;
              padding: 1rem;
            }
            .row {
              display: -ms-flexbox;
              display: flex;
              -ms-flex-wrap: wrap;
              flex-wrap: wrap;
              margin-right: -15px;
              margin-left: -15px;
            }    
            .table {
              width: 100%;
              margin-bottom: 1rem;
              background-color: transparent;
            }
            
            .table th,
            .table td {
              padding: 0.75rem;
              vertical-align: top;
              border-top: 1px solid #dee2e6;
            }
            
            .table thead th {
              vertical-align: bottom;
              border-bottom: 2px solid #dee2e6;
            }
            
            .table tbody + tbody {
              border-top: 2px solid #dee2e6;
            }
            
            .table .table {
              background-color: #fff;
            }
            .borderless td, .borderless th {
              border: none;
            } 
            .d-flex {
              display: -ms-flexbox !important;
              display: flex !important;
            }       
          </style>          
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  
  openModal(template: TemplateRef<any>,category_id) {
    this.category_idMentioned=category_id
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

 
  decline(): void {
    this.modalRef.hide();
  }

  doDeleteArticle(){
    this.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    var articleArr = {
      'article_id':this.articleArr.article_id,
      'category_id' : this.category_idMentioned
    };

    this.http.post<UserResponse>(this.url +'user/article/delete/category',articleArr,this.options)
    .subscribe((result: any) => {
      this.categoriesList = result.data;
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Category has been removed','timeout':3000}) 
      this.modalRef.hide();  
      this.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.modalRef.hide();
      this.progressRef.complete();
    });      
  }

  onSave($event) {
    this.alerts=[];
    if($event != 'failed'){
      this.categoriesList = $event.data.categories;
      this.articleArr.advalue_bw = $event.data.advalue_bw
      this.articleArr.advalue_fc = $event.data.advalue_fc
      this.childListener.notifyOther({option: 'save_article', value: $event.data});
      this.alerts.push({'type':'success','msg':'Article has been saved','timeout':3000})
    }else{
      this.alerts.push({'type':'danger','msg':'Article failed to saved','timeout':3000})
    }    
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  doSaveArticle(){
    this.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/subcategories-distinct/',this.options)
    .subscribe((result: any) => {
      this.saveArticleContainer.categoryList = result.results;
      if (this.saveArticleContainer.categoryList != null) {
        this.saveArticleContainer.categoryList = this.saveArticleContainer.categoryList.map(o => {
          return { checked: false, category_id: o.category_id };
        });
      }
      this.saveArticleContainer.articleArr= this.articleArr;
      this.saveArticleContainer.saveArticleModal.show();
      this.progressRef.complete();                          
    });        
  }

  docHandler(){
    this.progressRef.start();

    var url = 'http://pdf.antara-insight.id/?stat=1&article='+ this.articleArr.article_id +'&formate=0&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);

    this.progressRef.complete();
  }

  pdfHandler(){
    var url = 'http://pdf.antara-insight.id/?stat=2&article='+ this.articleArr.article_id +'&formate=1&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
  }

  ngOnInit() {
    this.progressRef = this.progress.ref();
  }

  textHandler(){
    var url = 'http://pdf.antara-insight.id/?stat=2&article='+ this.articleArr.article_id +'&formate=1&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
  }

  scanHandler(){
    var url = 'http://pdf.antara-insight.id/?stat=1&article='+ this.articleArr.article_id +'&formate=1&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
  }

  link(){
    var url = this.articleArr.file_pdf
    if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0){
      window.open(this.articleArr.file_pdf, '_blank')
    } else {
      alert("The news has no links")
    }
  }
  loadArticlesmap(article){
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>('./apidigivla/articles-by-id/?article_id='+ article.article_id)
    .subscribe((result: any) => { 
      var res =[];
      res = result.data;
      var argumenta =[];
      var args = res.join().split(',');   
      for (let i = 0; i< args.length;i++){
        var argsExp = args[i].split('" "')
        if (argsExp.length>1){
          for(let j=0;j<argsExp.length;j++){
            var re = new RegExp('"', 'gi');     
            var rplc = argsExp[j].replace(re, ""); 
            argumenta.push(rplc);
          }
        }else{
          var re = new RegExp('"', 'gi');     
          var rplc = args[i].replace(re, ""); 
          argumenta.push(rplc);
        }        
      }
      highlightedWord = argumenta.join();
      var re = new RegExp(',', 'gi');
      var highlightedWord = highlightedWord.replace(re, "||");

      if (article.summary==""||article.summary==undefined){
        this.summaryModel = article.content;
        this.contentModel = highlightedWord==''? this.highlighted(article.content,article.category_id) : this.highlighted(article.content,highlightedWord);
      }else{
        this.summaryModel= article.summary;
        this.contentModel = highlightedWord==''? this.highlighted(article.content,article.category_id) : this.highlighted(article.content,highlightedWord);
      } 
      this.pageDesc = {
        'print':false,
        'pdf_text':true,
        'doc':true,
        'save':true,
        'close':true,
        'scan_media':true,
        'text_print':true,
        'link':true
      };

      this.filePdfModel = article.file_pdf;
      this.mediaExtensionModel = article.file_pdf.split('.')[1];
      this.articleArr = article;
      this.titleModel = article.title;
      this.dateModel = article.datee;
      this.mediaModel = article.media_name;
      this.urlFileModel = "https://input.digivla.id/media_tv/"+ article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;    
      this.searchModal.show();
      this.ref.detectChanges();    
    },(err:any)=>{
    });   
  }
  highlighted(value: any, args: any): any {
    let argsArray=[];
    if (args.includes('&&')) { 
      argsArray = args.split('&&');         
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    
    for (let i=0; i < argsArray.length;i++){
      var re = new RegExp((argsArray[i].trimEnd()).trimStart(), 'gi');
      value = value.replace(re, "<mark>" + (argsArray[i].trimEnd()).trimStart() + "</mark>");  
    }
    return value;
  }
  highlightedsummary(value: any, args: any): any {
    let argsArray=[];
    if (args.includes('&&')) {
      argsArray = args.split('&&');
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    return value;
  }
}
