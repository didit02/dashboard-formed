import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleDetailsComponent } from './ArticleDetails.component';
import { SaveArticlesComponent } from './SaveArticles/SaveArticles.component';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgProgressModule } from '@ngx-progressbar/core';
import { ToasterModule} from 'angular2-toaster/angular2-toaster';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    NgProgressModule.forRoot(),
    ToasterModule
  ],
  declarations: [
    ArticleDetailsComponent,
    SaveArticlesComponent
  ],
  exports: [ ArticleDetailsComponent,
    SaveArticlesComponent ]
})
export class ArticleDetailsModule { }
