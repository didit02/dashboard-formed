import { Component, OnInit, Inject, NgZone, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgProgress } from "@ngx-progressbar/core";
import { environment } from "../../../environments/environment";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { ChildListenerService } from "../../views/Services/ChildListener.service";
import { Subscription } from "rxjs";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { ListenerService } from "../Services/listener.service";
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
am4core.useTheme(am4themes_animated);
@Component({
  selector: "app-influencerdashboard",
  templateUrl: "./influencerdashboard.component.html",
  styleUrls: ["./influencerdashboard.component.scss"]
})
export class InfluencerdashboardComponent implements OnInit {
  url = environment.apiUrl;
  article_idMentionModel;
  toneModel=0;
  category_idMentionModel;
  alerts;
  subscription: Subscription;
  influencername:any;
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token")
  });
  options = { headers: this.headers };
  influencerOpt = [];
  influencerQuotes = [];
  chartuse: any;
  paramsTime: any;
  loading: boolean = false;
  pageSize: number = 10;
  currentPage: number = 1;
  influencerQuotesrecords:any;
  contentmaps:any;
  loader:boolean=false;
  @ViewChild('editToneModal') public editToneModal;  
  constructor(
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
    private http: HttpClient,
    public progress: NgProgress,
    private zone: NgZone,
    private childListener: ChildListenerService,
    private listener: ListenerService
  ) {}
  getInfluencerQuotes() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
  }

  getInfluencerBox() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  ngOnInit() {
    this.parent.progressRef.start();
    this.paramsTime = {
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };
    this.rendatadataChart();
    this.renderdataquotes(this.currentPage);
    this.subscription = this.childListener.notifyObservable$.subscribe(res => {
      if (res.hasOwnProperty("option") && res.option === "call_child") {
        this.paramsTime = {
          start_date: moment(this.parent.periodFromModel)
            .format("YYYY-MM-DD")
            .toString(),
          end_date: moment(this.parent.periodEndModel)
            .format("YYYY-MM-DD")
            .toString()
        };
        this.rendatadataChart();
        this.renderdataquotes(this.currentPage)
      } else if (
        res.hasOwnProperty("option") &&
        res.option === "save_article"
      ) {
      }
    });
  }
  createSeries(field, name) {
    let series = this.chartuse.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueX = field;
    series.dataFields.categoryY = "influencer";
    series.name = name;
    series.columns.template.tooltipText = "[bold]{valueX}[/]";
    series.columns.template.height = am4core.percent(100);
    series.sequencedInterpolation = true;

    let valueLabel = series.bullets.push(new am4charts.LabelBullet());
    valueLabel.label.text = "{valueX}";
    valueLabel.label.horizontalCenter = "left";
    valueLabel.label.dx = 10;
    valueLabel.label.hideOversized = false;
    valueLabel.label.truncate = false;

    let categoryLabel = series.bullets.push(new am4charts.LabelBullet());
    categoryLabel.label.text = "";
    categoryLabel.label.horizontalCenter = "right";
    categoryLabel.label.dx = -10;
    categoryLabel.label.fill = am4core.color("#fff");
    categoryLabel.label.hideOversized = false;
    categoryLabel.label.truncate = false;
  }

  rendatadataChart() {
    this.loading = true;
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    this.chartuse = chart;
    interface UserResponse {
      data: Object;
    }

    let dataform = {
      page: 0,
      start_date: this.paramsTime.start_date,
      end_date: this.paramsTime.end_date
    };
    let datalistchart = [];
    this.http
      .post<UserResponse>(
        "https://demo.digivla.id/api/v1/influencer-count/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
        this.loading = false;
        this.influencerOpt = [];
        result["data"].forEach(element => {
          this.influencerOpt.push(element)
          let dataobj = {
            influencer: element.influencer_name,
            positive: element.count.positive,
            neutral: element.count.netral,
            negative: element.count.negative
          };
          datalistchart.push(dataobj);
        });
        chart.data = datalistchart;
        let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "influencer";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;
        let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.opposite = true;
        chart.legend = new am4charts.Legend();
        chart.colors.list = [
          am4core.color("#28A8D8"),
          am4core.color("#99B2DD"),
          am4core.color("#c60b3d")
        ];
        this.createSeries("positive", "Positive");
        this.createSeries("neutral", "Neutral");
        this.createSeries("negative", "Negative");
      },
      err => { 
        this.contentmaps = err.statusText;
        this.loading  = false;
        this.loader = true;
      }
      );

  }
  renderdataquotes(page){
    let dataparams = {
      page: page - 1,
      start_date: this.paramsTime.start_date,
      end_date: this.paramsTime.end_date,
      max_size:this.pageSize  
    }
    interface UserResponse {
      data: Object;
    }
    this.http
    .post<UserResponse>(
      "https://demo.digivla.id/api/v1/influencer/",
      dataparams,
      this.options
    )
    .subscribe((result: any) => {
      this.influencerQuotes = []; 
      this.influencerQuotesrecords = result.recordsTotal      
      result['data'].forEach(element => {
          this.influencerQuotes.push(element)
      });  
    });
  }
  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.renderdataquotes(this.currentPage)
  }
  xlsHandler(){
    let dataparams = {
      start_date: this.paramsTime.start_date,
      end_date: this.paramsTime.end_date,
    }
    interface UserResponse {
      data: Object;
    }
    this.http
    .post<UserResponse>(
      "https://demo.digivla.id/api/v1/download-excel/",
      dataparams,
      this.options
    )
    .subscribe((result: any) => {
      window.open(result.data)
      
    });

  }
  editTonemodals(article_id,category_id,tone , nameinfluencer){
    this.article_idMentionModel = article_id;
    this.category_idMentionModel = category_id;
    this.influencername = nameinfluencer
    this.toneModel = tone; 
    this.editToneModal.show();
  }
  doUpdateTone(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let editingParam={     
      "article_id": this.article_idMentionModel,
      "tone":this.toneModel,
      "influencer_name": this.influencername
    };
    this.http.post<UserResponse>( "https://demo.digivla.id/api/v1/update-tone/",editingParam,this.options)
    .subscribe((result: any) => {    
      this.influencerQuotes = [];   
      this.rendatadataChart();
      this.renderdataquotes(this.currentPage);
      this.editToneModal.hide();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, Tone failed to update.','timeout':3000}) 
      this.editToneModal.hide();
    });
  }
}
