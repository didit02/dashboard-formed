import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AlertModule } from 'ngx-bootstrap/alert';

import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import * as offlineExporting from 'highcharts/modules/offline-exporting.src';



// import { HighchartsChartModule } from 'highcharts-angular';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import * as highstock from 'highcharts/modules/stock.src';
import { InfluencerdashboardComponent } from './influencerdashboard.component';
import { InfluencerdashboardRoutingModule } from './influencerdashboard-routing.module';
import { NgxLoadingModule } from 'ngx-loading';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,

    InfluencerdashboardRoutingModule,

    ChartsModule,
    BsDropdownModule,
    CarouselModule.forRoot(),
    ButtonsModule.forRoot(),
    AlertModule.forRoot(),
    ChartModule,
    NgxLoadingModule.forRoot({}),
    // HighchartsChartModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
   
  ],
  declarations: [ InfluencerdashboardComponent ], 
  providers: [
    { provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting,offlineExporting,highstock ] } // add as factory to your providers
  ] 
})
// export class DashboardModule { }
export class InfluencerdashboardModule { }
