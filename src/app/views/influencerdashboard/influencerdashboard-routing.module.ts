import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';


import { InfluencerdashboardComponent } from './influencerdashboard.component';

const routes: Routes = [
  {
    path: '',
    component: InfluencerdashboardComponent,
    data: {
      title: 'influencerdashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfluencerdashboardRoutingModule {}