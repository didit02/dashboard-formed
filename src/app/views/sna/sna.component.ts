import { Component, OnInit, Inject } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { Subscription } from "rxjs";
import { ListenerService } from "../Services/listener.service";
import { ChildListenerService } from "../../views/Services/ChildListener.service";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { FormControl } from "@angular/forms";
@Component({
  selector: "app-sna",
  templateUrl: "./sna.component.html",
  styleUrls: ["./sna.component.scss"],
})
export class SnaComponent implements OnInit {
  InfluencerConfList = [];
  oripost: any;
  childerpost = [];
  subscription: Subscription;
  today = new Date();
  fcselecttype: FormControl;
  fcselectimage: FormControl;
  fcselectinfluencer: FormControl;
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  datagroupall = [];
  pushdataall = [];
  datause = {};
  dataalluse = [];
  contenttooltip: string;
  hovernode: boolean;
  datadropdown: any;
  topicgetdata: any;
  loading: boolean = false;
  nodataload: boolean = false;
  dataimage = [];
  paramsSna: any;
  imagenameget: any;
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token"),
  });
  options = { headers: this.headers };
  constructor(
    private statelocal: ListenerService,
    private http: HttpClient,
    private childListener: ChildListenerService,
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent
  ) {
    this.fcselecttype = new FormControl("");
    this.fcselectimage = new FormControl("");
    this.fcselectinfluencer = new FormControl("");
  }

  ngOnInit() {
    this.paramsSna = {
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString(),
    };
    this.fcselecttype.setValue("influencer");
    this.renderdatainit();
    this.subscription = this.childListener.notifyObservable$.subscribe(
      (res) => {
        if (res.hasOwnProperty("option") && res.option === "call_child") {
          this.paramsSna = {
            start_date: moment(this.parent.periodFromModel)
              .format("YYYY-MM-DD")
              .toString(),
            end_date: moment(this.parent.periodEndModel)
              .format("YYYY-MM-DD")
              .toString(),
          };
          this.fcselecttype.setValue("influencer");
          this.renderdatainit();
        }
      }
    );
    this.loadInfluencer();
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }
  renderdatainit() {
    this.dataimage = [];
    this.loading = true;
    interface UserResponse {
      data: Object;
    }
    let urluse;
    if (this.fcselecttype.value === "influencer") {
      urluse = "https://demo.digivla.id/api/v1/media-count/";
    } else {
      urluse = "https://demo.digivla.id/api/v1/get-sna/";
    }
    this.http
      .post<UserResponse>(urluse, this.paramsSna, this.options)
      .subscribe(
        (result: any) => {
          if (this.fcselecttype.value === "sna") {
            result["item_list"].forEach((element) => {
              this.InfluencerConfList.push(element);
            });
          }
          if (result.recordsTotal !== 0) {
            this.loading = false;
            this.nodataload = false;
          } else {
            this.loading = false;
            this.nodataload = true;
          }
          let dataarrnode = [];
          let testdata = [];
          result["data"].forEach((element) => {
            if (element.children.length !== 0) {
              element.children.forEach((element) => {
                if (element.linkWidth > 6) {
                  element.linkWidth = 6 ;
                }
                dataarrnode.push(element);
              });
              let dataobjpush = {
                name: result["data"][0].name,
                image: result["data"][0].image,
                children: dataarrnode,
              };
              this.dataimage.push(dataobjpush);
              this.rendersna();
            }
          });
        },
        (err: any) => {}
      );
  }

  rendersna() {
    let chart = am4core.create(
      "sna",
      am4plugins_forceDirected.ForceDirectedTree
    );
    let networkSeries = chart.series.push(
      new am4plugins_forceDirected.ForceDirectedSeries()
    );
    networkSeries.links.template.propertyFields.strokeWidth = "linkWidth";
    networkSeries.dataFields.linkWith = "linkWith";
    networkSeries.dataFields.name = "name";
    networkSeries.dataFields.id = "name";
    networkSeries.dataFields.value = "value";
    networkSeries.dataFields.children = "children";
    networkSeries.dataFields.color = "color";
    networkSeries.nodes.template.label.text = "{name}";
    networkSeries.fontSize = 12;
    networkSeries.nodes.template.label.fill = am4core.color("#000");
    networkSeries.nodes.template.label.valign = "bottom";
    networkSeries.nodes.template.circle.disabled = true;
    networkSeries.nodes.template.outerCircle.disabled = false;
    networkSeries.minRadius = 25;
    networkSeries.links.template.distance = 3;
    networkSeries.manyBodyStrength = -100;
    networkSeries.linkWithStrength = 1.5;
    let nodeTemplate = networkSeries.nodes.template;
    nodeTemplate.tooltipText = "{name}";
    nodeTemplate.fillOpacity = 1;

    // nodeTemplate.label.hideOversized = false;
    // nodeTemplate.label.truncate = true;
    var icon = networkSeries.nodes.template.createChild(am4core.Image);
    icon.propertyFields.href = "image";
    icon.horizontalCenter = "middle";
    icon.verticalCenter = "middle";

    let linkTemplate = networkSeries.links.template;
    linkTemplate.strokeWidth = 1;
    let linkHoverState = linkTemplate.states.create("hover");
    linkHoverState.properties.strokeOpacity = 1;
    linkHoverState.properties.strokeWidth = 2;
    nodeTemplate.events.on("over", function (event) {
      let dataItem = event.target.dataItem;
      dataItem.childLinks.each(function (link) {
        link.isHover = true;
      });
    });
    nodeTemplate.events.on("out", function (event) {
      let dataItem = event.target.dataItem;
      dataItem.childLinks.each(function (link) {
        link.isHover = false;
      });
    });
    networkSeries.data = this.dataimage;
  }
  changeselect() {
    if (this.fcselecttype.value === "influencer") {
      this.loadInfluencer();
    } else {
      this.InfluencerConfList = [];
    }
    this.renderdatainit();
    this.rendersna();
  }
  loadInfluencer() {
    this.InfluencerConfList = [];
    interface UserResponse {
      result: Object;
    }
    this.http
      .get<UserResponse>(
        "https://demo.digivla.id/api/v1/influencer-list/",
        this.options
      )
      .subscribe(
        (result: any) => {
          result["data"].forEach((element) => {
            this.InfluencerConfList.push(element.influencer_name);
          });
        },
        (err: any) => {}
      );
  }
  uploadimage() {
    let params = {
      name: this.fcselectinfluencer.value,
      image_for: this.fcselecttype.value,
      image_name: this.imagenameget.name,
      image: this.fcselectimage.value,
    };
    interface UserResponse {
      data: Object;
    }
    this.http
      .post<UserResponse>(
        "https://demo.digivla.id/api/v1/upload-image",
        params,
        this.options
      )
      .subscribe(
        (result: any) => {
          if (result.hasOwnProperty("image_name")) {
            if (this.fcselecttype.value === "influencer") {
              this.loadInfluencer();
            }
            this.renderdatainit();
            this.rendersna();
          }
        },
        (err: any) => {}
      );
  }
  processFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.imagenameget = file;
    var reader = new FileReader();
    reader.onloadend = () => {
      let base64 = reader.result;
      this.fcselectimage.setValue(base64);
    };
    reader.readAsDataURL(file);
  }
}
