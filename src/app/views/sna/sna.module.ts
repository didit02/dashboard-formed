import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SnaRoutingModule } from './sna-routing.module';
import { SnaComponent } from "./sna.component";
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    SnaRoutingModule,
    NgxLoadingModule.forRoot({}),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SnaComponent]
})
export class SnaModule { }
