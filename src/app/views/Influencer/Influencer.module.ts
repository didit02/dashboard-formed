import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfluencerComponent } from './Influencer.component';
import { InfluencerRoutingModule } from './Influencer-routing.module'

import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    InfluencerRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [InfluencerComponent]
})
export class InfluencerModule { }
