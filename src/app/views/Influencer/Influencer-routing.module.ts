import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { InfluencerComponent } from './Influencer.component';

const routes: Routes = [
  {
    path: '',
    component: InfluencerComponent,
    data: {
      title: 'Influencer'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfluencerRoutingModule {}
