import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import Containers
import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";
import { LoginComponent } from "./views/login/login.component";
// import { RegisterComponent } from './views/register/register.component';

import { AuthServiceService } from "./views/Services/AuthService.service";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full",
  },
  {
    path: "404",
    component: P404Component,
    data: {
      title: "Page 404",
    },
  },
  {
    path: "500",
    component: P500Component,
    data: {
      title: "Page 500",
    },
  },
  {
    path: "login",
    component: LoginComponent,
    data: {
      title: "Login Page",
    },
  },
  // {
  //   path: 'register',
  //   component: RegisterComponent,
  //   data: {
  //     title: 'Register Page'
  //   }
  // },
  {
    path: "",
    component: DefaultLayoutComponent,
    data: {
      title: "Home",
    },
    children: [
      // {
      //   path: 'base',
      //   loadChildren: './views/base/base.module#BaseModule'
      // },
      // {
      //   path: 'buttons',
      //   loadChildren: './views/buttons/buttons.module#ButtonsModule'
      // },
      // {
      //   path: 'charts',
      //   loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      // },
      {
        path: "dashboard",
        loadChildren: "./views/dashboard/dashboard.module#DashboardModule",
        canActivate: [AuthServiceService],
      },
      {
        path: "editing",
        loadChildren: "./views/Editing/Editing.module#EditingModule",
        canActivate: [AuthServiceService],
      },
      {
        path: "geospatial",
        loadChildren: "./views/Geospatial/Geospatial.module#GeospatialModule",
      },
      {
        path: "newstrends",
        loadChildren: "./views/Trends/Trends.module#TrendsModule",
      },
      // {
      //   path: 'icons',
      //   loadChildren: './views/icons/icons.module#IconsModule'
      // },
      {
        path: "newsclipping",
        loadChildren:
          "./views/NewsClipping/NewsClipping.module#NewsClippingModule",
        canActivate: [AuthServiceService],
      },
      {
        path: "groupmedia",
        loadChildren: "./views/GroupMedia/GroupMedia.module#GroupMediaModule",
      },
      {
        path: "groupcategory",
        loadChildren:
          "./views/GroupCategory/GroupCategory.module#GroupCategoryModule",
      },
      {
        path: "influencer",
        loadChildren: "./views/Influencer/Influencer.module#InfluencerModule",
      },
      {
        path: "subcategory",
        loadChildren:
          "./views/SubCategory/SubCategory.module#SubCategoryModule",
      },
      {
        path: "connect",
        loadChildren:
          "./views/JournalistConnect/JournalistConnect.module#JournalistConnectModule",
      },
      {
        path: "search",
        loadChildren: "./views/Search/Search.module#SearchModule",
        canActivate: [AuthServiceService],
      },
      {
        path: "influencerdashboard",
        loadChildren:
          "./views/influencerdashboard/influencerdashboard.module#InfluencerdashboardModule",
        // canActivate: [AuthServiceService]
      },
      {
        path: "maps",
        loadChildren: "./views/maps/maps.module#MapsModule",
        canActivate: [AuthServiceService],
      },
      {
        path: "sna",
        loadChildren: "./views/sna/sna.module#SnaModule",
        canActivate: [AuthServiceService],
      },
      // {
      //   path: 'theme',
      //   loadChildren: './views/theme/theme.module#ThemeModule'
      // },
      // {
      //   path: 'widgets',
      //   loadChildren: './views/widgets/widgets.module#WidgetsModule'
      // }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
