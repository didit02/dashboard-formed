import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { ModalModule } from "ngx-bootstrap";
import { NgxLoadingModule } from 'ngx-loading';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
// import { RegisterComponent } from './views/register/register.component';

import {AuthServiceService} from './views/Services/AuthService.service'

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AlertModule } from 'ngx-bootstrap/alert';
import { NgProgressModule } from '@ngx-progressbar/core';


@NgModule({
   imports: [
      ReactiveFormsModule,
      FormsModule,
      BrowserModule,
      AppRoutingModule,
      AppAsideModule,
      AppBreadcrumbModule.forRoot(),
      AppFooterModule,
      AppHeaderModule,
      AppSidebarModule,
      PerfectScrollbarModule,
      BsDropdownModule.forRoot(),
      TabsModule.forRoot(),
      ChartsModule,
      BsDatepickerModule.forRoot(),
      AlertModule.forRoot(),
      NgProgressModule.forRoot(),
      HttpClientModule,
      ModalModule.forRoot(),
      NgxLoadingModule.forRoot({}),
      InfiniteScrollModule
   ],
   declarations: [
      AppComponent,
      ...APP_CONTAINERS,
      P404Component,
      P500Component,
      LoginComponent,
      // InfluencerDashboardComponent,
      // InfluencerdashboardComponent,
      // RegisterComponent,
      // EnvironmentsComponent
   ],
   providers: [   
      AuthServiceService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
