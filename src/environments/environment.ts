// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //live prod
  apiUrl: 'https://api.digivla.id/api/v1/',
  apiSub: 'https://apijasamarga.digivla.id/api/v1/'
  // apiUrl:'http://10.10.30.24:8000/api/v1/'
  // Trinix local
  // apiUrl: 'http://192.168.1.12:8080/api/v1/',
  // local digivla
  // apiUrl: 'http://localhost:8080/api/v1/',
};
